package pe.gob.bnp.portalSNB.publicaciones.validation;

import pe.gob.bnp.portalSNB.publicaciones.dto.EventosDto;
import pe.gob.bnp.portalSNB.utilitary.common.Notification;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;

public class EventosValidation {
	
	
	private static String messageEventoSave = "No se encontraron datos del evento";
	
	private static String messageTituloSave = "No se encontro el titulo del evento";
	
	private static String messageBibliotecaSave = "No se encontro el id de la biblioteca";
	
	private static String messageRegistradorSave= "No se encontro el id del registrador";
	
	
	public static Notification validation(EventosDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageEventoSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getTitulo())) {
			notification.addError(messageTituloSave);
		}

		if (Utility.isEmptyOrNull(dto.getBibliotecaId())) {
			notification.addError(messageBibliotecaSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getRegistradorId())) {
			notification.addError(messageRegistradorSave);
		}
		
	
		return notification;
	}

}
