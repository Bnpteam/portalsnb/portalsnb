package pe.gob.bnp.portalSNB.publicaciones.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.portalSNB.publicaciones.dto.EventoDetalleResponse;
import pe.gob.bnp.portalSNB.publicaciones.dto.EventosAprobadosResponse;
import pe.gob.bnp.portalSNB.publicaciones.dto.EventosDto;
import pe.gob.bnp.portalSNB.publicaciones.dto.EventosListResponse;
import pe.gob.bnp.portalSNB.publicaciones.service.EventosService;

import pe.gob.bnp.portalSNB.utilitary.common.ResponseHandler;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api")
@Api(value = "/api/eventos")
@CrossOrigin(origins = "*")
public class EventosController {	
	
	private static final Logger logger = LoggerFactory.getLogger(EventosController.class);

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	EventosService eventosService;	
	
	/*
	 * EVENTOS
	 */
	
	@RequestMapping(method = RequestMethod.POST, path = "/eventos", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar un evento", response = ResponseTransaction.class, responseContainer = "Set", httpMethod = "POST")
	public ResponseEntity<Object> save(
			@ApiParam(value = "Estructura JSON de curso BNP", required = true)
			@RequestBody EventosDto eventoDto)
			throws IOException {
		ResponseTransaction response = new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = eventosService.save(eventoDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos save eventos: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/eventos IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/eventos Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT,path="/evento/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar un evento", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de evento ", required = true)
			@PathVariable Long id,@RequestBody EventosDto eventosDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = eventosService.update(id,eventosDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos put eventos: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/evento/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/evento/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	

	@RequestMapping(method = RequestMethod.GET, path="/evento/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "obtener evento x Id", response= EventoDetalleResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id evento", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();		
			
			response = eventosService.get(id);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get evento: " + tiempo);
			if (response == null || response.getList().size()==0) {
				logger.info("/evento/"+id+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/evento/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/evento/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/evento/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}	
	
	
	@RequestMapping(method = RequestMethod.GET, path="/eventos/{idRegistrador}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar eventos BNP", response= EventosListResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@ApiParam(value = "idRegistrador", required = true)
			@PathVariable Long idRegistrador,
			@RequestParam(value = "titulo", defaultValue = "", required=false) String  titulo,
			@RequestParam(value = "codedepartamento", defaultValue = "", required=false) String  codedepartamento,
			@RequestParam(value = "idEstado", defaultValue = "", required=false) String  idEstado){
		ResponseTransaction response=new ResponseTransaction();
		try { 
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();		
			
			response = eventosService.searchAll(idRegistrador,titulo,codedepartamento,idEstado);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get eventos registrador: " + tiempo);
			
			if (response == null || response.getList().size()==0 ) {
				logger.info("/eventos/idRegistrador/ "+idRegistrador+" titulo:"+titulo+", codedepartamento: "+codedepartamento+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/eventos/idRegistrador/"+idRegistrador+" titulo:"+titulo+", codedepartamento: "+codedepartamento+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/eventos/idRegistrador/"+idRegistrador+" titulo:"+titulo+", codedepartamento: "+codedepartamento+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/eventos/idRegistrador/"+idRegistrador+" titulo:"+titulo+", codedepartamento: "+codedepartamento+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
		
	@RequestMapping(method = RequestMethod.GET, path="/eventosaprobados",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar eventos aprobados BNP", response= EventosAprobadosResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAprobados(
			@RequestParam(value = "titulo", defaultValue = "", required=false) String  titulo,
			@RequestParam(value = "codedepartamento", defaultValue = "", required=false) String  codedepartamento){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();		
			
			response = eventosService.searchAprobados(titulo,codedepartamento);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get eventos aprobados: " + tiempo);
			
			if (response == null || response.getList().size()==0 ) {
				logger.info("/eventosaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/eventosaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/eventosaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/eventosaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, path="/eventoaprobado/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "obtener evento aprobado x Id", response= EventoDetalleResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getAprobado(
			@ApiParam(value = "id evento", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();		
			
			response = eventosService.getAprobado(id);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get eventos aprobado id: " + tiempo);
			
			if (response == null || response.getList().size()==0) {
				logger.info("/eventoaprobado/"+id+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/eventoaprobado/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/eventoaprobado/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/eventoaprobado/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}	


}
