package pe.gob.bnp.portalSNB.publicaciones.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroAllRequest;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroRequest;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.repository.BaseRepository;

@Repository
public interface NoticiasRepository extends BaseRepository<Publicaciones>{	
	
	public ResponseTransaction getNoticiaId();
	
	public ResponseTransaction read(Long id);
	
	public ResponseTransaction readAprobado(Long id);
	
	public ResponseTransaction listAprobados(FiltroRequest filtroRequest);
	
	public ResponseTransaction listAll(FiltroAllRequest filtroRequest);

}