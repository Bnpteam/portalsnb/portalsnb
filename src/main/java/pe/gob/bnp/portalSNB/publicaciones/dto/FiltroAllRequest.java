package pe.gob.bnp.portalSNB.publicaciones.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FiltroAllRequest {	
	
	private String idRegistrador;	
	
	private String codeDepartamento;
	
	private String titulo;
	
	private String estadoId;	

}
