package pe.gob.bnp.portalSNB.publicaciones.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticiaDetalleResponse {
	
	private String id;
	
	private String titulo;
	
	private String detalle;
	
	private String bibliotecaId;
	
	private String biblioteca;
	
	private String registradorId;
	
	private String registrador;
	
	private String horaInicio;	
	
	private String horaFin;
	
	private String fechaInicio;
	
	private String fechaFin;
	
	private String costo;
	
	private String cantidadMaxima;
	
	private String paginaWeb;
	
	private String correo;
	
	private String telefono;
	
	private String direccion;
	
	private String referencia;
	
	private String inscripcion;
	
	private String inscripcionFechaInicio;
	
	private String inscripcionFechaFin;
	
	private String codeDepartamento;
	
	private String departamento;
	
	private String codeProvincia;
	
	private String provincia;	
	
	private String codeDistrito;
	
	private String distrito;
	
	private String linkFoto;	
	
}
