package pe.gob.bnp.portalSNB.publicaciones.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventosAprobadosResponse {

	private String id;

	private String titulo;

	private String detalle;

	private String idBiblioteca;

	private String nombreBiblioteca;

	private String horaInicio;

	private String horaFin;

	private String fechaInicio;

	private String fechaFin;

	private String codeDepartamento;

	private String departamento;

	private String foto;

}
