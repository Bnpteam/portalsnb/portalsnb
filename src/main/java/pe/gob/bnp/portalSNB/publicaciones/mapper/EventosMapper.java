package pe.gob.bnp.portalSNB.publicaciones.mapper;

import org.springframework.stereotype.Component;
import pe.gob.bnp.portalSNB.publicaciones.dto.EventosDto;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;

@Component
public class EventosMapper {
	
	public Publicaciones reverseMapperEventoSave(EventosDto eventosDto) {
		Publicaciones publicaciones= new Publicaciones();
		
		publicaciones.setId(Utility.parseStringToLong(eventosDto.getId()));
		publicaciones.setTitulo(eventosDto.getTitulo());
		publicaciones.setDetalle(eventosDto.getDetalle());
		publicaciones.setCategoriaId(Utility.parseStringToLong(eventosDto.getCategoriaId()));
		publicaciones.setBibliotecaId(Utility.parseStringToLong(eventosDto.getBibliotecaId()));
		publicaciones.setRegistradorId(Utility.parseStringToLong(eventosDto.getRegistradorId()));
		publicaciones.setHoraInicio(eventosDto.getHoraInicio());
		publicaciones.setHoraFin(eventosDto.getHoraFin());
		publicaciones.setFechaInicio(Utility.parseStringToDate(eventosDto.getFechaInicio()));
		publicaciones.setFechaFin(Utility.parseStringToDate(eventosDto.getFechaFin()));
		publicaciones.setCosto(eventosDto.getCosto());
		publicaciones.setCantidadMaxima(eventosDto.getCantidadMaxima());
		publicaciones.setPaginaWeb(eventosDto.getPaginaWeb());
		publicaciones.setCorreo(eventosDto.getCorreo());
		publicaciones.setTelefono(eventosDto.getTelefono());
		publicaciones.setDireccion(eventosDto.getDireccion());
		publicaciones.setReferencia(eventosDto.getReferencia());
		publicaciones.setInscripcion(eventosDto.getInscripcion());
		publicaciones.setInscripcionFechaInicio(Utility.parseStringToDate(eventosDto.getInscripcionFechaInicio()));
		publicaciones.setInscripcionFechaFin(Utility.parseStringToDate(eventosDto.getInscripcionFechaFin()));
		publicaciones.setCodeDepartamento(eventosDto.getCodeDepartamento());
		publicaciones.setCodeProvincia(eventosDto.getCodeProvincia());
		publicaciones.setCodeDistrito(eventosDto.getCodeDistrito());		
		publicaciones.setCreatedUser(eventosDto.getUsuarioRegistroId());
		
		return publicaciones;
	}
	
	public Publicaciones reverseMapperEventoUpdate(EventosDto eventosDto) {
		Publicaciones publicaciones= new Publicaciones();
		
		publicaciones.setId(Utility.parseStringToLong(eventosDto.getId()));
		publicaciones.setTitulo(eventosDto.getTitulo());
		publicaciones.setDetalle(eventosDto.getDetalle());
		publicaciones.setCategoriaId(Utility.parseStringToLong(eventosDto.getCategoriaId()));
		publicaciones.setBibliotecaId(Utility.parseStringToLong(eventosDto.getBibliotecaId()));
		publicaciones.setRegistradorId(Utility.parseStringToLong(eventosDto.getRegistradorId()));
		publicaciones.setHoraInicio(eventosDto.getHoraInicio());
		publicaciones.setHoraFin(eventosDto.getHoraFin());
		publicaciones.setFechaInicio(Utility.parseStringToDate(eventosDto.getFechaInicio()));
		publicaciones.setFechaFin(Utility.parseStringToDate(eventosDto.getFechaFin()));
		publicaciones.setCosto(eventosDto.getCosto());
		publicaciones.setCantidadMaxima(eventosDto.getCantidadMaxima());
		publicaciones.setPaginaWeb(eventosDto.getPaginaWeb());
		publicaciones.setCorreo(eventosDto.getCorreo());
		publicaciones.setTelefono(eventosDto.getTelefono());
		publicaciones.setDireccion(eventosDto.getDireccion());
		publicaciones.setReferencia(eventosDto.getReferencia());
		publicaciones.setInscripcion(eventosDto.getInscripcion());
		publicaciones.setInscripcionFechaInicio(Utility.parseStringToDate(eventosDto.getInscripcionFechaInicio()));
		publicaciones.setInscripcionFechaFin(Utility.parseStringToDate(eventosDto.getInscripcionFechaFin()));
		publicaciones.setCodeDepartamento(eventosDto.getCodeDepartamento());
		publicaciones.setCodeProvincia(eventosDto.getCodeProvincia());
		publicaciones.setCodeDistrito(eventosDto.getCodeDistrito());		
		publicaciones.setUpdatedUser(eventosDto.getUsuarioModificaId());
		
		return publicaciones;
	}

}
