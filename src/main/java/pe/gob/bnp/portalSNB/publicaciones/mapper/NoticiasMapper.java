package pe.gob.bnp.portalSNB.publicaciones.mapper;

import org.springframework.stereotype.Component;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasDto;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;

@Component
public class NoticiasMapper {
	
	public Publicaciones reverseMapperNoticiaSave(NoticiasDto noticiasDto) {
		Publicaciones publicaciones= new Publicaciones();
		
		publicaciones.setId(Utility.parseStringToLong(noticiasDto.getId()));
		publicaciones.setTitulo(noticiasDto.getTitulo());
		publicaciones.setDetalle(noticiasDto.getDetalle());	
		publicaciones.setBibliotecaId(Utility.parseStringToLong(noticiasDto.getBibliotecaId()));
		publicaciones.setRegistradorId(Utility.parseStringToLong(noticiasDto.getRegistradorId()));
		publicaciones.setHoraInicio(noticiasDto.getHoraInicio());
		publicaciones.setHoraFin(noticiasDto.getHoraFin());
		publicaciones.setFechaInicio(Utility.parseStringToDate(noticiasDto.getFechaInicio()));
		publicaciones.setFechaFin(Utility.parseStringToDate(noticiasDto.getFechaFin()));
		publicaciones.setCosto(noticiasDto.getCosto());
		publicaciones.setCantidadMaxima(noticiasDto.getCantidadMaxima());
		publicaciones.setPaginaWeb(noticiasDto.getPaginaWeb());
		publicaciones.setCorreo(noticiasDto.getCorreo());
		publicaciones.setTelefono(noticiasDto.getTelefono());
		publicaciones.setDireccion(noticiasDto.getDireccion());
		publicaciones.setReferencia(noticiasDto.getReferencia());
		publicaciones.setInscripcion(noticiasDto.getInscripcion());
		publicaciones.setInscripcionFechaInicio(Utility.parseStringToDate(noticiasDto.getInscripcionFechaInicio()));
		publicaciones.setInscripcionFechaFin(Utility.parseStringToDate(noticiasDto.getInscripcionFechaFin()));
		publicaciones.setCodeDepartamento(noticiasDto.getCodeDepartamento());
		publicaciones.setCodeProvincia(noticiasDto.getCodeProvincia());
		publicaciones.setCodeDistrito(noticiasDto.getCodeDistrito());		
		publicaciones.setCreatedUser(noticiasDto.getUsuarioRegistroId());
		
		return publicaciones;
	}
	
	public Publicaciones reverseMapperNoticiaUpdate(NoticiasDto noticiasDto) {
		Publicaciones publicaciones= new Publicaciones();
		
		publicaciones.setId(Utility.parseStringToLong(noticiasDto.getId()));
		publicaciones.setTitulo(noticiasDto.getTitulo());
		publicaciones.setDetalle(noticiasDto.getDetalle());	
		publicaciones.setBibliotecaId(Utility.parseStringToLong(noticiasDto.getBibliotecaId()));
		publicaciones.setRegistradorId(Utility.parseStringToLong(noticiasDto.getRegistradorId()));
		publicaciones.setHoraInicio(noticiasDto.getHoraInicio());
		publicaciones.setHoraFin(noticiasDto.getHoraFin());
		publicaciones.setFechaInicio(Utility.parseStringToDate(noticiasDto.getFechaInicio()));
		publicaciones.setFechaFin(Utility.parseStringToDate(noticiasDto.getFechaFin()));
		publicaciones.setCosto(noticiasDto.getCosto());
		publicaciones.setCantidadMaxima(noticiasDto.getCantidadMaxima());
		publicaciones.setPaginaWeb(noticiasDto.getPaginaWeb());
		publicaciones.setCorreo(noticiasDto.getCorreo());
		publicaciones.setTelefono(noticiasDto.getTelefono());
		publicaciones.setDireccion(noticiasDto.getDireccion());
		publicaciones.setReferencia(noticiasDto.getReferencia());
		publicaciones.setInscripcion(noticiasDto.getInscripcion());
		publicaciones.setInscripcionFechaInicio(Utility.parseStringToDate(noticiasDto.getInscripcionFechaInicio()));
		publicaciones.setInscripcionFechaFin(Utility.parseStringToDate(noticiasDto.getInscripcionFechaFin()));
		publicaciones.setCodeDepartamento(noticiasDto.getCodeDepartamento());
		publicaciones.setCodeProvincia(noticiasDto.getCodeProvincia());
		publicaciones.setCodeDistrito(noticiasDto.getCodeDistrito());		
		publicaciones.setUpdatedUser(noticiasDto.getUsuarioModificaId());
		
		return publicaciones;
	}

}
