package pe.gob.bnp.portalSNB.publicaciones.model;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pe.gob.bnp.portalSNB.utilitary.model.BaseEntity;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Publicaciones extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 0L;

	private String titulo;

	private String detalle;

	private Long categoriaId;

	private Long bibliotecaId;

	private Long registradorId;

	private String horaInicio;

	private String horaFin;

	private Date fechaInicio;

	private Date fechaFin;

	private String costo;

	private String cantidadMaxima;

	private String paginaWeb;

	private String correo;

	private String telefono;

	private String direccion;

	private String referencia;

	private String inscripcion;

	private Date inscripcionFechaInicio;

	private Date inscripcionFechaFin;

	private String codeDepartamento;

	private String codeProvincia;

	private String codeDistrito;

	private String rutaFoto;

	private String nombreFoto;

	private String extensionFoto;

}
