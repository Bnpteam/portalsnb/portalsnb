package pe.gob.bnp.portalSNB.publicaciones.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiaDetalleResponse;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasAprobadosResponse;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasDto;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasListResponse;
import pe.gob.bnp.portalSNB.publicaciones.service.NoticiasService;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseHandler;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api")
@Api(value = "/api/noticias")
@CrossOrigin(origins = "*")
public class NoticiasController {	
	
	private static final Logger logger = LoggerFactory.getLogger(NoticiasController.class);

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	NoticiasService noticiasService;
	
	
	/*
	 * NOTICIAS
	 */
	
	@RequestMapping(method = RequestMethod.POST, path = "/noticias", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar una noticia", response = ResponseTransaction.class, responseContainer = "Set", httpMethod = "POST")
	public ResponseEntity<Object> save(
			@ApiParam(value = "Estructura JSON de curso BNP", required = true)
			@RequestBody NoticiasDto noticiasDto)
			throws IOException {
		ResponseTransaction response = new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();		
			
			response = noticiasService.save(noticiasDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos post noticias: " + tiempo);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/noticias IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/noticias Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT,path="/noticia/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar una noticia", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de noticia ", required = true)
			@PathVariable Long id,@RequestBody NoticiasDto noticiasDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();		
			
			response = noticiasService.update(id,noticiasDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos put noticias: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/noticia/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/noticia/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
		
	
	@RequestMapping(method = RequestMethod.GET, path="/noticia/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "obtener noticia x Id", response= NoticiaDetalleResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id noticia", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();		
			
			response = noticiasService.get(id);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get noticia: " + tiempo);
			if (response == null || response.getList().size()==0) {
				logger.info("/noticia/"+id+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/noticia/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/noticia/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/noticia/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/noticiaaprobado/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "obtener noticia aprobada x Id", response= NoticiaDetalleResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getAprobado(
			@ApiParam(value = "id noticia", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();	
			
			response = noticiasService.getAprobado(id);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get noticiaaprobado: " + tiempo);
			
			if (response == null || response.getList().size()==0) {
				logger.info("/noticiaaprobado/"+id+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/noticiaaprobado/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/noticiaaprobado/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/noticiaaprobado/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}	
	
	
	@RequestMapping(method = RequestMethod.GET, path="/noticiasaprobados",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar noticias aprobados BNP", response= NoticiasAprobadosResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAprobados(
			@RequestParam(value = "titulo", defaultValue = "", required=false) String  titulo,
			@RequestParam(value = "codedepartamento", defaultValue = "", required=false) String  codedepartamento){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();	
			
			response = noticiasService.searchAprobados(titulo,codedepartamento);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get noticiaaprobadoss list: " + tiempo);
			if (response == null || response.getList().size()==0 ) {
				logger.info("/noticiasaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+"No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/noticiasaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/noticiasaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/noticiasaprobados titulo:"+titulo+", codedepartamento: "+codedepartamento+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/noticias/{idRegistrador}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar noticias BNP", response= NoticiasListResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@ApiParam(value = "idRegistrador", required = true)
			@PathVariable Long idRegistrador,
			@RequestParam(value = "titulo", defaultValue = "", required=false) String  titulo,
			@RequestParam(value = "codedepartamento", defaultValue = "", required=false) String  codedepartamento,
			@RequestParam(value = "idEstado", defaultValue = "", required=false) String  idEstado){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();	
			
			response = noticiasService.searchAll(idRegistrador,titulo,codedepartamento,idEstado);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get noticia registrador list: " + tiempo);
			
			if (response == null || response.getList().size()==0 ) {
				logger.info("/noticias/"+idRegistrador+" titulo:"+titulo+", codedepartamento: "+codedepartamento+",idEstado: "+idEstado+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/noticias/"+idRegistrador+"  titulo:"+titulo+", codedepartamento: "+codedepartamento+",idEstado: "+idEstado+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/noticias/"+idRegistrador+"  titulo:"+titulo+", codedepartamento: "+codedepartamento+",idEstado: "+idEstado+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/noticias/"+idRegistrador+"  titulo:"+titulo+", codedepartamento: "+codedepartamento+",idEstado: "+idEstado+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}

}
