package pe.gob.bnp.portalSNB.publicaciones.service;

import java.net.MalformedURLException;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import pe.gob.bnp.portalSNB.config.shared.dto.SharedResponse;
import pe.gob.bnp.portalSNB.config.shared.repository.SharedRepository;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroAllRequest;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroRequest;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasDto;
import pe.gob.bnp.portalSNB.publicaciones.exception.NoticiasException;
import pe.gob.bnp.portalSNB.publicaciones.mapper.NoticiasMapper;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.publicaciones.repository.NoticiasRepository;
import pe.gob.bnp.portalSNB.publicaciones.validation.NoticiasValidation;

import pe.gob.bnp.portalSNB.publicaciones.service.ImagesSaveProccess;
import pe.gob.bnp.portalSNB.utilitary.common.Notification;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;

@Service
public class NoticiasService {

	private static final String NOMBRE_FOTO = "foto";

	private String codeSuccess = "0000";

	private String noticiaId, nombreFoto;

	private byte[] fotoBytes;

	private SharedResponse sharedConfig;

	private ResponseTransaction response;

	@Autowired
	NoticiasRepository noticiasRepository;

	@Autowired
	SharedRepository sharedRepository;

	@Autowired
	NoticiasMapper noticiasMapper;

	private void readConfig() {

		try {
			response = sharedRepository.read();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
				List<Object> sharedList = response.getList();
				if (!sharedList.isEmpty()) {
					Object sharedObject = sharedList.get(0);
					if (sharedObject instanceof SharedResponse) {
						sharedConfig = (SharedResponse) sharedObject;
					}
				}
			}

		} catch (Exception ex) {
			response.setCodeResponse("0666");
			response.setResponse(ex.getMessage());
		}
	}

	public ResponseTransaction save(NoticiasDto noticiasDto) {
		
		try {

			inicializarVariables();

			inicializarConfiguracion();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				inicializarValidacionesDto(noticiasDto);

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				inicializarValidacionesFoto(noticiasDto);

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				obtenerNoticiaId();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				guardarFotoServidor();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				guardar(noticiasDto);

		} catch (Exception ex) {
			response.setCodeResponse("0666");
			response.setResponse(ex.getMessage());
			return response;
		}

		return NoticiasException.setMessageResponseSave(response);

	}

	public ResponseTransaction update(Long id, NoticiasDto noticiasDto) {

		try {

			inicializarVariables();

			inicializarConfiguracion();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				inicializarValidacionesDto(noticiasDto);

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				if (!Utility.isEmptyOrNull(noticiasDto.getFoto())) {
					inicializarValidacionesFoto(noticiasDto);
				}

			noticiaId = String.valueOf(id);

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				if (!Utility.isEmptyOrNull(noticiasDto.getFoto())) {
					actualizarFotoServidor();
				}

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				actualizar(noticiasDto);

		} catch (Exception ex) {
			response.setCodeResponse("0666");
			response.setResponse(ex.getMessage());
			return response;
		}

		return NoticiasException.setMessageResponseUpdate(response);

	}

	public ResponseTransaction get(Long id) {

		ResponseTransaction response = noticiasRepository.read(id);

		return response;
	}
	
	public ResponseTransaction getAprobado(Long id) {

		ResponseTransaction response = noticiasRepository.readAprobado(id);

		return response;
	}
	
	public ResponseTransaction searchAprobados(String titulo, String codedepartamento){	

		ResponseTransaction response = new ResponseTransaction();	
		
		FiltroRequest filtroRequest = new FiltroRequest ();
		filtroRequest.setTitulo(titulo);
		filtroRequest.setCodeDepartamento(codedepartamento);
		
		response=noticiasRepository.listAprobados(filtroRequest);
		
		return NoticiasException.setMessageAprobadosList(response);	
	}
	
	public ResponseTransaction searchAll(Long idRegistrador,String titulo, String codedepartamento,String idEstado){	

		ResponseTransaction response = new ResponseTransaction();	
		
		FiltroAllRequest filtroRequest = new FiltroAllRequest ();
		filtroRequest.setIdRegistrador(String.valueOf(idRegistrador));
		filtroRequest.setTitulo(titulo);
		filtroRequest.setCodeDepartamento(codedepartamento);
		filtroRequest.setEstadoId(idEstado);
		response=noticiasRepository.listAll(filtroRequest);
		
		return NoticiasException.setMessageAllList(response);	
	}

	private void inicializarVariables() {

		response = new ResponseTransaction();
		noticiaId = "";
		nombreFoto = "";
	}

	private void inicializarConfiguracion() {

		readConfig();

		if (sharedConfig == null) {
			response.setCodeResponse("0090");
			response.setResponse("Error al leer la configuración del servidor compartido");
		}
	}

	private void inicializarValidacionesDto(NoticiasDto noticiasDto) {

		Notification notification = NoticiasValidation.validation(noticiasDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
		}
	}

	private void inicializarValidacionesFoto(NoticiasDto noticiasDto) {

		try {
			fotoBytes = Base64.getDecoder().decode(noticiasDto.getFoto());
		} catch (Exception ex) {
			response.setCodeResponse("0091");
			response.setResponse("Error al decodificar la imagen...");

		}
	}

	private void obtenerNoticiaId() {

		response = noticiasRepository.getNoticiaId();

		if (response != null && response.getCodeResponse() != null && response.getCodeResponse().equals(codeSuccess)) {

			noticiaId = response.getId();

		}
	}

	private void guardarFotoServidor() {

		nombreFoto = NOMBRE_FOTO + noticiaId;

		response = saveSharedServer();
	}

	private void actualizarFotoServidor() {

		nombreFoto = NOMBRE_FOTO + noticiaId;

		response = updateSharedServer();
	}

	private void guardar(NoticiasDto noticiasDto) {

		noticiasDto.setId(noticiaId);

		Publicaciones resource = noticiasMapper.reverseMapperNoticiaSave(noticiasDto);

		response = noticiasRepository.persist(resource);

	}

	private void actualizar(NoticiasDto noticiasDto) {

		noticiasDto.setId(noticiaId);

		Publicaciones resource = noticiasMapper.reverseMapperNoticiaUpdate(noticiasDto);

		response = noticiasRepository.update(resource);

	}

	private ResponseTransaction saveSharedServer() {

		NtlmPasswordAuthentication authentication = null;

		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),
					sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}

		try {
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getResourceFolder() + noticiaId;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				smbDirectoy.mkdirs();
			}

			ImagesSaveProccess fotoProccess = new ImagesSaveProccess(nombreFoto, pathDirectory, fotoBytes,
					authentication);

			fotoProccess.start();

		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Server Window Save:" + ex);
			return response;
		}

		response.setCodeResponse(codeSuccess);
		response.setResponse("Shared Server Succesfull.");
		return response;

	}

	private ResponseTransaction updateSharedServer() {

		NtlmPasswordAuthentication authentication = null;

		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),
					sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}

		try {
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getResourceFolder() + noticiaId;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				response.setCodeResponse("0084");
				response.setResponse("Error el directorio" + pathDirectory + " no existe");
				return response;
			}

			if (!Utility.isEmptyOrNull(nombreFoto)) {
				ImagesUpdateProccess cajera1 = new ImagesUpdateProccess(nombreFoto, pathDirectory, fotoBytes,authentication);
				cajera1.start();
			}

		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Server Window Save:" + ex);
			return response;
		}

		response.setCodeResponse(codeSuccess);
		response.setResponse("Shared Server Succesfull.");
		return response;

	}

}
