package pe.gob.bnp.portalSNB.publicaciones.service;

import java.net.MalformedURLException;
import java.util.Base64;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import pe.gob.bnp.portalSNB.config.shared.dto.SharedResponse;
import pe.gob.bnp.portalSNB.config.shared.repository.SharedRepository;
import pe.gob.bnp.portalSNB.publicaciones.dto.EventosDto;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroAllRequest;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroRequest;
import pe.gob.bnp.portalSNB.publicaciones.exception.EventosException;
import pe.gob.bnp.portalSNB.publicaciones.mapper.EventosMapper;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.publicaciones.repository.EventosRepository;
import pe.gob.bnp.portalSNB.publicaciones.validation.EventosValidation;
import pe.gob.bnp.portalSNB.publicaciones.service.ImagesSaveProccess;
import pe.gob.bnp.portalSNB.utilitary.common.Notification;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;

@Service
public class EventosService {
	
	private static final String NOMBRE_FOTO= "foto";

	private String codeSuccess = "0000";
	
	private String eventoId,nombreFoto;

	private byte[] fotoBytes;
	
	private SharedResponse sharedConfig;
	
	private ResponseTransaction response;

	@Autowired
	EventosRepository eventosRepository;

	@Autowired
	SharedRepository sharedRepository;

	@Autowired
	EventosMapper eventoMapper;
	

	private void readConfig() {
		
		try {
			response = sharedRepository.read();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
				List<Object> sharedList = response.getList();
				if (!sharedList.isEmpty()) {
					Object sharedObject = sharedList.get(0);
					if (sharedObject instanceof SharedResponse) {
						sharedConfig = (SharedResponse) sharedObject;
					}
				}
			}

		} catch (Exception ex) {
			response.setCodeResponse("0666");			
			response.setResponse(ex.getMessage());
		}
	}

	public ResponseTransaction save(EventosDto eventosDto) {
		try {
			
			inicializarVariables();
			
			inicializarConfiguracion();
			
			if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) inicializarValidacionesDto(eventosDto);
			
			if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) inicializarValidacionesFoto(eventosDto);
			
			if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) obtenerEventoId();
			
			if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) guardarFotoServidor();
			
			if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) guardar(eventosDto);		
			
		}catch(Exception ex) {
			response.setCodeResponse("0666");			
			response.setResponse(ex.getMessage());
			return response;
		}	

		return EventosException.setMessageResponseSave(response);
		
	}	
	
	
	public ResponseTransaction update(Long id, EventosDto eventosDto) {

		try {

			inicializarVariables();

			inicializarConfiguracion();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				inicializarValidacionesDto(eventosDto);

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				if (!Utility.isEmptyOrNull(eventosDto.getFoto())) {
					inicializarValidacionesFoto(eventosDto);
				}

			eventoId = String.valueOf(id);

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				if (!Utility.isEmptyOrNull(eventosDto.getFoto())) {
					actualizarFotoServidor();
				}

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				actualizar(eventosDto);

		} catch (Exception ex) {
			response.setCodeResponse("0666");
			response.setResponse(ex.getMessage());
			return response;
		}

		return EventosException.setMessageResponseUpdate(response);

	}
	
	
	public ResponseTransaction get(Long id)  {
	
		ResponseTransaction response = eventosRepository.read(id);

		return response;
	}
	
	public ResponseTransaction getAprobado(Long id)  {
		
		ResponseTransaction response = eventosRepository.readAprobado(id);

		return response;
	}
	
	
	public ResponseTransaction searchAll(Long idRegistrador,String titulo, String codedepartamento,String idEstado){	

		ResponseTransaction response = new ResponseTransaction();	
		
		FiltroAllRequest filtroAllRequest = new FiltroAllRequest ();
		filtroAllRequest.setIdRegistrador(String.valueOf(idRegistrador));
		filtroAllRequest.setTitulo(titulo);
		filtroAllRequest.setCodeDepartamento(codedepartamento);
		filtroAllRequest.setEstadoId(idEstado);
		response=eventosRepository.listAll(filtroAllRequest);
		
		return EventosException.setMessageAllList(response);	
	}
	
	
	public ResponseTransaction searchAprobados(String titulo, String codedepartamento){	

		ResponseTransaction response = new ResponseTransaction();	
		
		FiltroRequest filtroRequest = new FiltroRequest ();
		filtroRequest.setTitulo(titulo);
		filtroRequest.setCodeDepartamento(codedepartamento);
		
		response=eventosRepository.listAprobados(filtroRequest);
		
		return EventosException.setMessageAprobadosList(response);	
	}
	
	
	
	
	private void inicializarVariables() {
		
		response = new ResponseTransaction();
		eventoId="";
		nombreFoto="";	
	}
		
	private void inicializarConfiguracion() {
		
		readConfig();

		if (sharedConfig == null) {
			response.setCodeResponse("0090");
			response.setResponse("Error al leer la configuración del servidor compartido");			
		}	
	}
		
	private void inicializarValidacionesDto(EventosDto eventosDto) {
		
		Notification notification = EventosValidation.validation(eventosDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());			
		}		
	}
	
	private void inicializarValidacionesFoto(EventosDto eventosDto) {
		
		try {
			fotoBytes = Base64.getDecoder().decode(eventosDto.getFoto());
		}catch(Exception ex) {
			response.setCodeResponse("0091");
			response.setResponse("Error al decodificar la imagen...");
			
		}
	}
		
	private void obtenerEventoId() {
		
		response = eventosRepository.getEventoId();
		
		if(response != null && response.getCodeResponse() != null && response.getCodeResponse().equals(codeSuccess)) {
			
			eventoId=response.getId();
			
		}
	}
	
	private void guardarFotoServidor() {
		
		nombreFoto=NOMBRE_FOTO+eventoId;
		
		response=saveSharedServer();
	}
	
	private void actualizarFotoServidor() {

		nombreFoto = NOMBRE_FOTO + eventoId;

		response = updateSharedServer();
	}
	
	private void guardar(EventosDto eventosDto) {
		
		eventosDto.setId(eventoId);
		
		Publicaciones resource = eventoMapper.reverseMapperEventoSave(eventosDto);
		
		response = eventosRepository.persist(resource);
		
	}	
	
	private void actualizar(EventosDto eventosDto) {

		eventosDto.setId(eventoId);

		Publicaciones resource = eventoMapper.reverseMapperEventoUpdate(eventosDto);

		response = eventosRepository.update(resource);

	}

	private ResponseTransaction saveSharedServer(){
		
		NtlmPasswordAuthentication authentication = null;
		
		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(), sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}

		try {			
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getCourseFolder() + eventoId;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				smbDirectoy.mkdirs();
			}		
			
			ImagesSaveProccess fotoProccess = new ImagesSaveProccess(nombreFoto, pathDirectory,fotoBytes, authentication);

			fotoProccess.start();		
			
		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Window Server guardar:" + ex);
			return response;
		}		

		response.setCodeResponse(codeSuccess);
		response.setResponse("Servidor Compartido Exitoso.");
		return response;

	}
	
	private ResponseTransaction updateSharedServer() {

		NtlmPasswordAuthentication authentication = null;

		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),
					sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}

		try {
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getCourseFolder() + eventoId;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				response.setCodeResponse("0084");
				response.setResponse("Error el directorio" + pathDirectory + " no existe");
				return response;
			}

			if (!Utility.isEmptyOrNull(nombreFoto)) {
				ImagesUpdateProccess cajera1 = new ImagesUpdateProccess(nombreFoto, pathDirectory, fotoBytes,authentication);
				cajera1.start();
			}

		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Window Server actualizar:" + ex);
			return response;
		}

		response.setCodeResponse(codeSuccess);
		response.setResponse("Servidor Compartido Exitoso.");
		return response;

	}

}
