package pe.gob.bnp.portalSNB.publicaciones.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FiltroRequest {
		
	private String codeDepartamento;
	
	private String titulo;
	
}
