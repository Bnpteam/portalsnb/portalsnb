package pe.gob.bnp.portalSNB.publicaciones.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.portalSNB.config.DataSource;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroAllRequest;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroRequest;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiaDetalleResponse;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasAprobadosResponse;
import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasListResponse;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.publicaciones.repository.NoticiasRepository;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;
import pe.gob.bnp.portalSNB.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class NoticiasRepositoryJDBC extends BaseJDBCOperation implements NoticiasRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(NoticiasRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Publicaciones entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PUBLICACIONES.SP_PERSIST_NOTICIA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*25*/
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_TITULO", Utility.getString(entity.getTitulo()));			
			cstm.setString("P_DETALLE",Utility.getString(entity.getDetalle()));			
			cstm.setLong("P_BIBLIOTECA_ID",entity.getBibliotecaId());
			cstm.setLong("P_REGISTRADOR_ID",entity.getRegistradorId());
			cstm.setString("P_HORA_INICIO",Utility.getString(entity.getHoraInicio()));
			cstm.setString("P_HORA_FIN",Utility.getString(entity.getHoraFin()));
			cstm.setDate("P_FECHA_INICIO",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getFechaInicio())));
			cstm.setDate("P_FECHA_FIN",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getFechaFin())));
			cstm.setString("P_COSTO",Utility.getString(entity.getCosto()));
			cstm.setString("P_CANTIDAD_MAXIMA",Utility.getString(entity.getCantidadMaxima()));
			cstm.setString("P_PAGINA_WEB",Utility.getString(entity.getPaginaWeb()));
			cstm.setString("P_CORREO",Utility.getString(entity.getCorreo()));
			cstm.setString("P_TELEFONO",Utility.getString(entity.getTelefono()));
			cstm.setString("P_DIRECCION",Utility.getString(entity.getDireccion()));
			cstm.setString("P_REFERENCIA",Utility.getString(entity.getReferencia()));
			cstm.setString("P_INSCRIPCION",Utility.getString(entity.getInscripcion()));
			cstm.setDate("P_INS_FECHA_INICIO",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getInscripcionFechaInicio())));
			cstm.setDate("P_INS_FECHA_FIN",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getInscripcionFechaFin())));
			cstm.setString("P_CODE_DEPARTAMENTO",Utility.getString(entity.getCodeDepartamento()));
			cstm.setString("P_CODE_PROVINCIA",Utility.getString(entity.getCodeProvincia()));
			cstm.setString("P_CODE_DISTRITO",Utility.getString(entity.getCodeDistrito()));			
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_NOTICIA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_NOTICIA_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_PUBLICACIONES.SP_PERSIST_NOTICIA: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction update(Publicaciones entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PUBLICACIONES.SP_UPDATE_NOTICIA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*25*/
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_TITULO", Utility.getString(entity.getTitulo()));			
			cstm.setString("P_DETALLE",Utility.getString(entity.getDetalle()));			
			cstm.setLong("P_BIBLIOTECA_ID",entity.getBibliotecaId());
			cstm.setLong("P_REGISTRADOR_ID",entity.getRegistradorId());
			cstm.setString("P_HORA_INICIO",Utility.getString(entity.getHoraInicio()));
			cstm.setString("P_HORA_FIN",Utility.getString(entity.getHoraFin()));
			cstm.setDate("P_FECHA_INICIO",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getFechaInicio())));
			cstm.setDate("P_FECHA_FIN",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getFechaFin())));
			cstm.setString("P_COSTO",Utility.getString(entity.getCosto()));
			cstm.setString("P_CANTIDAD_MAXIMA",Utility.getString(entity.getCantidadMaxima()));
			cstm.setString("P_PAGINA_WEB",Utility.getString(entity.getPaginaWeb()));
			cstm.setString("P_CORREO",Utility.getString(entity.getCorreo()));
			cstm.setString("P_TELEFONO",Utility.getString(entity.getTelefono()));
			cstm.setString("P_DIRECCION",Utility.getString(entity.getDireccion()));
			cstm.setString("P_REFERENCIA",Utility.getString(entity.getReferencia()));
			cstm.setString("P_INSCRIPCION",Utility.getString(entity.getInscripcion()));
			cstm.setDate("P_INS_FECHA_INICIO",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getInscripcionFechaInicio())));
			cstm.setDate("P_INS_FECHA_FIN",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getInscripcionFechaFin())));
			cstm.setString("P_CODE_DEPARTAMENTO",Utility.getString(entity.getCodeDepartamento()));
			cstm.setString("P_CODE_PROVINCIA",Utility.getString(entity.getCodeProvincia()));
			cstm.setString("P_CODE_DISTRITO",Utility.getString(entity.getCodeDistrito()));			
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));			
			cstm.registerOutParameter("S_NOTICIA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_NOTICIA_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_PUBLICACIONES.SP_UPDATE_NOTICIA: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction getNoticiaId() {
	
        String dBTransaction = "{ Call PKG_API_PUBLICACIONES.SP_GET_NOTICIA_ID(?,?)}";
        ResponseTransaction response = new ResponseTransaction();  
		
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
					
			cstm.registerOutParameter("S_NOTICIA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);		
			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_NOTICIA_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_PUBLICACIONES.SP_GET_NOTICIA_ID: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction read(Long id) {
	
        String dBTransaction = "{ Call PKG_API_PUBLICACIONES.SP_GET_NOTICIA(?,?,?)}";
             
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
	
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_NOTICIA_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					NoticiaDetalleResponse resourceResponse = new NoticiaDetalleResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("NOTICIA_ID")));
					resourceResponse.setTitulo(Utility.getString(rs.getString("NOTICIA_TITULO")));
					resourceResponse.setDetalle(Utility.getString(rs.getString("NOTICIA_DETALLE")));
					resourceResponse.setBibliotecaId(Utility.parseLongToString(rs.getLong("BIBLIOTECA_ID")));
					resourceResponse.setBiblioteca(Utility.getString(rs.getString("BIBLIOTECA")));
					resourceResponse.setRegistradorId(Utility.parseLongToString(rs.getLong("REGISTRADOR_ID")));
					resourceResponse.setRegistrador(Utility.getString(rs.getString("REGISTRADOR")));
					resourceResponse.setHoraInicio(Utility.getString(rs.getString("HORA_INICIO")));
					resourceResponse.setHoraFin(Utility.getString(rs.getString("HORA_FIN")));
					resourceResponse.setFechaInicio(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_INICIO"))));
					resourceResponse.setFechaFin(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_FIN"))));
					
					resourceResponse.setCosto(Utility.getString(rs.getString("COSTO")));
					resourceResponse.setCantidadMaxima(Utility.getString(rs.getString("CANTIDAD_MAXIMA")));
					resourceResponse.setPaginaWeb(Utility.getString(rs.getString("PAGINA_WEB")));
					resourceResponse.setCorreo(Utility.getString(rs.getString("CORREO")));
					resourceResponse.setTelefono(Utility.getString(rs.getString("TELEFONO")));
					resourceResponse.setDireccion(Utility.getString(rs.getString("DIRECCION")));
					resourceResponse.setReferencia(Utility.getString(rs.getString("REFERENCIA")));
					
					resourceResponse.setInscripcion(Utility.getString(rs.getString("INSCRIPCION")));
					resourceResponse.setInscripcionFechaInicio(Utility.parseDateToString(Utility.getDate(rs.getDate("INS_FECHA_INICIO"))));
					resourceResponse.setInscripcionFechaFin(Utility.parseDateToString(Utility.getDate(rs.getDate("INS_FECHA_FIN"))));
					
					resourceResponse.setCodeDepartamento(Utility.getString(rs.getString("CODE_DEPARTAMENTO")));
					resourceResponse.setDepartamento(Utility.getString(rs.getString("DEPARTAMENTO")));
					resourceResponse.setCodeProvincia(Utility.getString(rs.getString("CODE_PROVINCIA")));
					resourceResponse.setProvincia(Utility.getString(rs.getString("PROVINCIA")));
					resourceResponse.setCodeDistrito(Utility.getString(rs.getString("CODE_DISTRITO")));
					resourceResponse.setDistrito(Utility.getString(rs.getString("DISTRITO")));					
					resourceResponse.setLinkFoto(Utility.getString(rs.getString("LINK_FOTO")));
				
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_PUBLICACIONES.SP_GET_NOTICIA: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());	
		}
		return response;	
	}

	@Override
	public ResponseTransaction listAprobados(FiltroRequest filtroRequest) {
		
        String dBTransaction = "{ Call PKG_API_PUBLICACIONES.SP_LIST_NOTICIAS_APROBADOS(?,?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_TITULO", Utility.getString(filtroRequest.getTitulo()));			
			cstm.setString("P_CODDEPARTAMENTO",Utility.getString(filtroRequest.getCodeDepartamento()));				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {	
					NoticiasAprobadosResponse noticiasResponse = new NoticiasAprobadosResponse();
					noticiasResponse.setId(Utility.parseLongToString(rs.getLong("NOTICIA_ID")));
					noticiasResponse.setTitulo(Utility.getString(rs.getString("NOTICIA_TITULO")));
					noticiasResponse.setDetalle(Utility.getString(rs.getString("NOTICIA_DETALLE")));
					noticiasResponse.setIdBiblioteca(Utility.parseLongToString(rs.getLong("ID_BIBLIOTECA")));
					noticiasResponse.setNombreBiblioteca(Utility.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					noticiasResponse.setHoraInicio(Utility.getString(rs.getString("HORA_INICIO")));
					noticiasResponse.setHoraFin(Utility.getString(rs.getString("HORA_FIN")));
					noticiasResponse.setFechaInicio(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_INICIO"))));
					noticiasResponse.setFechaFin(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_FIN"))));
					noticiasResponse.setCodeDepartamento(Utility.getString(rs.getString("CODE_DEPARTAMENTO")));
					noticiasResponse.setDepartamento(Utility.getString(rs.getString("DEPARTAMENTO")));
					noticiasResponse.setFoto(Utility.getString(rs.getString("LINK_FOTO")));
					slidersResponse.add(noticiasResponse);
				}	
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_PUBLICACIONES.SP_LIST_NOTICIAS_APROBADOS: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction listAll(FiltroAllRequest filtroRequest) {
		
        String dBTransaction = "{ Call PKG_API_PUBLICACIONES.SP_LIST_NOTICIAS(?,?,?,?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();       
        
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_TITULO", Utility.getString(filtroRequest.getTitulo()));			
			cstm.setString("P_CODDEPARTAMENTO",Utility.getString(filtroRequest.getCodeDepartamento()));		
			cstm.setLong("P_ESTADO_ID",Utility.parseStringToLong(filtroRequest.getEstadoId()));	
			cstm.setLong("P_REGISTRADOR_ID",Utility.parseStringToLong(filtroRequest.getIdRegistrador()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {	
					NoticiasListResponse noticiasResponse = new NoticiasListResponse();
					noticiasResponse.setId(Utility.parseLongToString(rs.getLong("NOTICIA_ID")));
					noticiasResponse.setTitulo(Utility.getString(rs.getString("NOTICIA_TITULO")));
					noticiasResponse.setDetalle(Utility.getString(rs.getString("NOTICIA_DETALLE")));
					noticiasResponse.setIdBiblioteca(Utility.parseLongToString(rs.getLong("ID_BIBLIOTECA")));
					noticiasResponse.setNombreBiblioteca(Utility.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					noticiasResponse.setHoraInicio(Utility.getString(rs.getString("HORA_INICIO")));
					noticiasResponse.setHoraFin(Utility.getString(rs.getString("HORA_FIN")));
					noticiasResponse.setFechaInicio(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_INICIO"))));
					noticiasResponse.setFechaFin(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_FIN"))));
					noticiasResponse.setCodeDepartamento(Utility.getString(rs.getString("CODE_DEPARTAMENTO")));
					noticiasResponse.setDepartamento(Utility.getString(rs.getString("DEPARTAMENTO")));
					noticiasResponse.setFoto(Utility.getString(rs.getString("LINK_FOTO")));
					noticiasResponse.setIdEstado(Utility.parseLongToString(rs.getLong("ESTADO_ID")));
					noticiasResponse.setEstado(Utility.getString(rs.getString("ESTADO_DESCRIPCION")));
					slidersResponse.add(noticiasResponse);
				}	
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_PUBLICACIONES.SP_LIST_NOTICIAS: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	@Override
	public ResponseTransaction readAprobado(Long id) {
		
        String dBTransaction = "{ Call PKG_API_PUBLICACIONES.SP_GET_NOTICIAAPROBADO(?,?,?)}";
             
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();    
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_NOTICIA_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					NoticiaDetalleResponse resourceResponse = new NoticiaDetalleResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("NOTICIA_ID")));
					resourceResponse.setTitulo(Utility.getString(rs.getString("NOTICIA_TITULO")));
					resourceResponse.setDetalle(Utility.getString(rs.getString("NOTICIA_DETALLE")));
					resourceResponse.setBibliotecaId(Utility.parseLongToString(rs.getLong("BIBLIOTECA_ID")));
					resourceResponse.setBiblioteca(Utility.getString(rs.getString("BIBLIOTECA")));
					resourceResponse.setRegistradorId(Utility.parseLongToString(rs.getLong("REGISTRADOR_ID")));
					resourceResponse.setRegistrador(Utility.getString(rs.getString("REGISTRADOR")));
					resourceResponse.setHoraInicio(Utility.getString(rs.getString("HORA_INICIO")));
					resourceResponse.setHoraFin(Utility.getString(rs.getString("HORA_FIN")));
					resourceResponse.setFechaInicio(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_INICIO"))));
					resourceResponse.setFechaFin(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_FIN"))));
					
					resourceResponse.setCosto(Utility.getString(rs.getString("COSTO")));
					resourceResponse.setCantidadMaxima(Utility.getString(rs.getString("CANTIDAD_MAXIMA")));
					resourceResponse.setPaginaWeb(Utility.getString(rs.getString("PAGINA_WEB")));
					resourceResponse.setCorreo(Utility.getString(rs.getString("CORREO")));
					resourceResponse.setTelefono(Utility.getString(rs.getString("TELEFONO")));
					resourceResponse.setDireccion(Utility.getString(rs.getString("DIRECCION")));
					resourceResponse.setReferencia(Utility.getString(rs.getString("REFERENCIA")));
					
					resourceResponse.setInscripcion(Utility.getString(rs.getString("INSCRIPCION")));
					resourceResponse.setInscripcionFechaInicio(Utility.parseDateToString(Utility.getDate(rs.getDate("INS_FECHA_INICIO"))));
					resourceResponse.setInscripcionFechaFin(Utility.parseDateToString(Utility.getDate(rs.getDate("INS_FECHA_FIN"))));
					
					resourceResponse.setCodeDepartamento(Utility.getString(rs.getString("CODE_DEPARTAMENTO")));
					resourceResponse.setDepartamento(Utility.getString(rs.getString("DEPARTAMENTO")));
					resourceResponse.setCodeProvincia(Utility.getString(rs.getString("CODE_PROVINCIA")));
					resourceResponse.setProvincia(Utility.getString(rs.getString("PROVINCIA")));
					resourceResponse.setCodeDistrito(Utility.getString(rs.getString("CODE_DISTRITO")));
					resourceResponse.setDistrito(Utility.getString(rs.getString("DISTRITO")));					
					resourceResponse.setLinkFoto(Utility.getString(rs.getString("LINK_FOTO")));
				
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_PUBLICACIONES.SP_GET_NOTICIA: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			System.out.println(e);
			
		}
		return response;
	}


}
