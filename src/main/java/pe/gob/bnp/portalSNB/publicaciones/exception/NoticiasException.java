package pe.gob.bnp.portalSNB.publicaciones.exception;

import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;

public class NoticiasException {
	
	public static final String error9999 = "Error 9999: No se ejecutó ninguna transacción.";
	public static final String error0002 = "El departamento ingresado no encontrado.";
	public static final String error0003 = "La provincia ingresada no encontrada.";
	public static final String error0004 = "El distrito ingresado no encontrado.";
	public static final String error0005 = "El id de la biblioteca no encontrado.";
	public static final String error0006 = "El id del registrador no encontrado.";
	
	
	public static ResponseTransaction setMessageResponseSave(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}

		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}

		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}

		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}

		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006);
		}


		return response;
	}
	
	public static ResponseTransaction setMessageAprobadosList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}		
		
		return response;
	}
	
	public static ResponseTransaction setMessageAllList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}		
		
		return response;
	}


}
