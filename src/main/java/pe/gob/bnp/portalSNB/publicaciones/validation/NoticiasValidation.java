package pe.gob.bnp.portalSNB.publicaciones.validation;

import pe.gob.bnp.portalSNB.publicaciones.dto.NoticiasDto;
import pe.gob.bnp.portalSNB.utilitary.common.Notification;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;

public class NoticiasValidation {
	
	private static String messageNoticiaSave = "No se encontraron datos de la noticia";
	
	private static String messageTituloSave = "No se encontro el titulo de la noticia";
	
	private static String messageBibliotecaSave = "No se encontro el id de la biblioteca";
	
	private static String messageRegistradorSave= "No se encontro el id del registrador";
	
	
	public static Notification validation(NoticiasDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageNoticiaSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getTitulo())) {
			notification.addError(messageTituloSave);
		}

		if (Utility.isEmptyOrNull(dto.getBibliotecaId())) {
			notification.addError(messageBibliotecaSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getRegistradorId())) {
			notification.addError(messageRegistradorSave);
		}
		
	
		return notification;
	}

}
