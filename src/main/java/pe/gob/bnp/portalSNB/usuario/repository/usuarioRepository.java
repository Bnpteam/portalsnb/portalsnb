package pe.gob.bnp.portalSNB.usuario.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.portalSNB.usuario.dto.UsuarioLogin;
import pe.gob.bnp.portalSNB.usuario.model.Usuario;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.repository.BaseRepository;
@Repository
public interface usuarioRepository extends BaseRepository<Usuario> {
	public ResponseTransactionSimple validarUsuario(UsuarioLogin usuarioLogin);

	public ResponseTransactionSimple validarTrabajador(UsuarioLogin usuarioLogin);
}
