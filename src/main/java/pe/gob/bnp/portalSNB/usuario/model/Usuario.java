package pe.gob.bnp.portalSNB.usuario.model;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
	private String codUsuario;
	private String nombreUsuario;
	private String tipoUsuario;
	private List<BibliotecaPortalDto> bibliotecas;
	
	public Usuario() {
		this.bibliotecas = new ArrayList<>();
	}
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public List<BibliotecaPortalDto> getBibliotecas() {
		return bibliotecas;
	}
	public void setBibliotecas(List<BibliotecaPortalDto> bibliotecas) {
		this.bibliotecas = bibliotecas;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	
	
	
}
