package pe.gob.bnp.portalSNB.usuario.exception;

import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;

public class usuarioException {
	public static final  String error9999 ="Error 9999: No se ejecutó ninguna transacción.";
	public static final  String error0001 ="Error 0001: El nombre del Usuario ya se encuentra registrado.";
	public static final  String error0002 ="Error 0002: El dominio del Usuario ya se encuentra registrado.";
	public static final  String error0003 ="Error 0003: No se puede realizar la actualización: El id del Usuario no pertenece a ningun usuario registrado.";

	public static final  String error0002Login ="Error 0002:El dominio ingresado no existe.";
	public static final  String error0003Login ="Error 0003: El usuario esta inactivo.";

	public static ResponseTransactionSimple setMessageResponseLogin(ResponseTransactionSimple response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Login);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Login);
		}

		return response;
	}
}
