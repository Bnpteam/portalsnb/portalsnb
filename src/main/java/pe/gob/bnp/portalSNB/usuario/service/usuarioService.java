package pe.gob.bnp.portalSNB.usuario.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.portalSNB.usuario.dto.UsuarioLogin;
import pe.gob.bnp.portalSNB.usuario.exception.usuarioException;
import pe.gob.bnp.portalSNB.usuario.repository.usuarioRepository;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
@Service
public class usuarioService {
	@Autowired
	usuarioRepository usuarioRepository;
	
	public ResponseTransactionSimple login(UsuarioLogin usuarioLogin){
		ResponseTransactionSimple response = new ResponseTransactionSimple();
		if(usuarioLogin.getUserbnp()!=1) {
			response = usuarioRepository.validarUsuario(usuarioLogin);
		}
		else {
			response = validarUsuarioAD(usuarioLogin);
		}
			
		return usuarioException.setMessageResponseLogin(response);				
		

	}
	
	public ResponseTransactionSimple validarUsuarioAD(UsuarioLogin usuarioLogin) {
		ResponseTransactionSimple response = new ResponseTransactionSimple();
		Hashtable credenciales = new Hashtable(11);
		DirContext contexto = null;
		try {
			credenciales.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			credenciales.put(Context.SECURITY_AUTHENTICATION,"simple");
			credenciales.put(Context.SECURITY_PRINCIPAL, usuarioLogin.getUsuario()+"@bnp.gob.pe");
			credenciales.put(Context.SECURITY_CREDENTIALS, usuarioLogin.getPassword());
			credenciales.put(Context.PROVIDER_URL, "ldap://172.16.88.203:389");
			
			contexto = new InitialDirContext(credenciales);	
			System.out.print("Contexto"+ contexto);
			if (contexto!=null) {
				response = usuarioRepository.validarTrabajador(usuarioLogin);
				System.out.println("USUARIO CORRECTO");  
			}
		} catch (NamingException e) {			

			System.out.println(this.obtenerMotivoRechazoEnAD(new String(e.toString())));
			
		} finally {
			if (contexto!=null) {
				try {
					contexto.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		
		return usuarioException.setMessageResponseLogin(response);	
	}
	
	
	public String obtenerMotivoRechazoEnAD(String cadena) {
		int pos = cadena.indexOf("data ");
		String descripcionRechazo = "";
		if (pos>0) {
			String campo1= cadena.substring(pos+5,pos+5+3).concat("");
			if (campo1.equals("52e")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "El nombre de usuario o contraseÃ±a no es correcto.";
			}	
			if (campo1.equals("775")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "La cuenta a que se hace referencia estÃ¡ bloqueada y no se puede utilizar.";
			}			
			if (campo1.equals("525â€‹")){
				descripcionRechazo = "Usuario no existe.";
			}

			if (campo1.equals("530â€‹") || campo1.equals("531â€‹")){
				descripcionRechazo = "Intento de logueo no permitido.";
			}			
			if (campo1.equals("532â€‹")){
				descripcionRechazo = "ContraseÃ±a expirada.";
			}	
			if (campo1.equals("533â€‹")){
				descripcionRechazo = "Cuenta deshabilitada.";
			}			
			if (campo1.equals("701â€‹")){
				descripcionRechazo = "Cuenta expirada.";
			}
			if (campo1.equals("773â€‹")){
				descripcionRechazo = "Debe resetear su contraseÃ±a.";
			}				
		

		}else {
			descripcionRechazo = "No se puede obtener descripción de rechazo";
		}
		return descripcionRechazo;
	}	
}
