package pe.gob.bnp.portalSNB.usuario.dto;

public class UsuarioLogin {

	private String usuario;
	private String password;
	private int userbnp;
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getUserbnp() {
		return userbnp;
	}
	public void setUserbnp(int userbnp) {
		this.userbnp = userbnp;
	}
	
	
	
}
