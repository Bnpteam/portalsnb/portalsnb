package pe.gob.bnp.portalSNB.usuario.model;

public class BibliotecaPortalDto {
	private String id;
	//private String year;
	private String nombre;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
