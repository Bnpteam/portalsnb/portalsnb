package pe.gob.bnp.portalSNB.usuario.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.stereotype.Repository;
import oracle.jdbc.OracleTypes;
import pe.gob.bnp.portalSNB.usuario.dto.UsuarioLogin;
import pe.gob.bnp.portalSNB.usuario.model.BibliotecaPortalDto;
import pe.gob.bnp.portalSNB.usuario.model.Usuario;
import pe.gob.bnp.portalSNB.usuario.repository.usuarioRepository;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.repository.jdbc.BaseJDBCOperation;
@Repository
public class usuarioRepositoryJDBC extends BaseJDBCOperation implements usuarioRepository {

	@Override
	public ResponseTransaction persist(Usuario entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction update(Usuario entity) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public ResponseTransactionSimple validarTrabajador(UsuarioLogin usuarioLogin) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PERSONA.SP_AUTENTICAR_TRABAJADOR(?,?,?,?,?)}";
        ResponseTransactionSimple response = new ResponseTransactionSimple();
        Usuario usuario = new Usuario();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_USUARIO", usuarioLogin.getUsuario());
			cstm.setString("E_PASSWORD", usuarioLogin.getPassword());
			cstm.registerOutParameter("S_USUARIO_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_NOMBRE_COMPLETO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			//cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);			

			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				String id=  cstm.getString("S_USUARIO_ID");
				String nombres = cstm.getString("S_NOMBRE_COMPLETO");
				usuario.setNombreUsuario(nombres);
				usuario.setCodUsuario(id);
				usuario.setTipoUsuario("1");
//				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
//				while (rs.next()) {
//					BibliotecaPortalDto biblioteca = new BibliotecaPortalDto();
//					biblioteca.setId(rs.getString("BIBLIOTECA_ID"));					
//					// biblioteca.setYear(Util.getString(rs.getString("ANIO_DECLARACION")));
//					biblioteca.setNombre(rs.getString("NOMBRE"));
//					usuario.getBibliotecas().add(biblioteca);
//				}
				response.setList(usuario);
				
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		
		return response;
	}
	
	@Override
	public ResponseTransactionSimple validarUsuario(UsuarioLogin usuarioLogin) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PERSONA.SP_LOGIN_PORTAL_WEB(?,?,?,?,?,?)}";
        ResponseTransactionSimple response = new ResponseTransactionSimple();
        Usuario usuario = new Usuario();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_USUARIO", usuarioLogin.getUsuario());
			cstm.setString("E_PASSWORD", usuarioLogin.getPassword());
			cstm.registerOutParameter("S_REGISTRADOR_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_NOMBRE_COMPLETO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);			

			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				String id=  cstm.getString("S_REGISTRADOR_ID");
				String nombres = cstm.getString("S_NOMBRE_COMPLETO");
				usuario.setNombreUsuario(nombres);
				usuario.setCodUsuario(id);
				usuario.setTipoUsuario("0");
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					BibliotecaPortalDto biblioteca = new BibliotecaPortalDto();
					biblioteca.setId(rs.getString("BIBLIOTECA_ID"));					
					// biblioteca.setYear(Util.getString(rs.getString("ANIO_DECLARACION")));
					biblioteca.setNombre(rs.getString("NOMBRE"));
					usuario.getBibliotecas().add(biblioteca);
				}
				response.setList(usuario);
				
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		
		return response;
	}

}
