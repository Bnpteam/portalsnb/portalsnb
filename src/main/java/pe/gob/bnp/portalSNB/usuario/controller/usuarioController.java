package pe.gob.bnp.portalSNB.usuario.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.portalSNB.usuario.dto.UsuarioLogin;
import pe.gob.bnp.portalSNB.usuario.service.usuarioService;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseHandler;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;

@RestController
@RequestMapping("/api")
@Api(value="/api/usuario")
@CrossOrigin(origins = "*")
public class usuarioController {
	
	private static final Logger logger = LoggerFactory.getLogger(usuarioController.class);
	
	@Autowired
	ResponseHandler responseHandler;

	
	@Autowired
	usuarioService usuarioService; 
	
	@RequestMapping(method = RequestMethod.POST, path = "/loginUsuario", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Login de usuario, debe estar habilitado domain", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 400, message = "Solicitud contiene errores"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})
	public ResponseEntity<Object> login(
			@ApiParam(value = "Login de usuario userbnp 0 si no es dominio , 1 si es dominio", required = true)
			@RequestBody  UsuarioLogin usuarioLogin){ 
		ResponseTransactionSimple response=new ResponseTransactionSimple();
		try {
			
			response = usuarioService.login(usuarioLogin);
			
			
			return this.responseHandler.getOkResponseTransactionSimple(response);
		} catch (IllegalArgumentException ex) {
			logger.error("login IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponseSimple(response,"Error Autenticacion "+ex.getMessage());
		} catch (Throwable ex) {
			logger.error("login Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponseSimple(response,ex);
		}
	
	}	
	
}
