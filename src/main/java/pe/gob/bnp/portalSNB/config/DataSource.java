package pe.gob.bnp.portalSNB.config;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataSource {
	
	private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    /*static {
        config.setJdbcUrl( "jdbc:oracle:thin:@172.16.88.154:1521:dbdev" );
        config.setUsername( "TIENDA_VIRTUAL" );
        config.setPassword( "dev_tiendav2711ual#" );
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        ds = new HikariDataSource( config );
    }*/
    
    static {
        config.setJdbcUrl( "jdbc:oracle:thin:@172.16.88.154:1521:dbdev" );
        config.setUsername( "RNB" );
        config.setPassword( "dev_rnbsd23x" );
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        ds = new HikariDataSource( config );
    }

    private DataSource() {}

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
