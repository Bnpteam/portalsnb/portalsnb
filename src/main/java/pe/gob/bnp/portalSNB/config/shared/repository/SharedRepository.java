package pe.gob.bnp.portalSNB.config.shared.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;

@Repository
public interface SharedRepository {
	
	public ResponseTransaction read();

}
