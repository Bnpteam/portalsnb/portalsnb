package pe.gob.bnp.portalSNB.config.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SharedResponse {
	
	private String id;
	
	private String domain;
	
	private String user;
	
	private String password;
	
	private String server;
	
	private String http;	
	
	private String folder;
	
	private String resourceFolder;
	
	private String courseFolder;
}
