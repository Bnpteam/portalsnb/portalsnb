package pe.gob.bnp.portalSNB.maestras.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import oracle.jdbc.OracleTypes;
import pe.gob.bnp.portalSNB.maestras.modal.listmaestras;
import pe.gob.bnp.portalSNB.maestras.modal.maestras;
import pe.gob.bnp.portalSNB.maestras.repository.maestrasRepository;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class maestrasRepositoryJDBC extends BaseJDBCOperation implements maestrasRepository {
	

	private static final Logger logger = LoggerFactory.getLogger(maestrasRepositoryJDBC.class);	
	

	@Override
	public ResponseTransaction persist(maestras entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction update(maestras entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransactionSimple getMaestras(String tipoMaestras) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_LISTA_X_TIPO(?,?,?)}";
        ResponseTransactionSimple response = new ResponseTransactionSimple();
        maestras maestras = new maestras();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_TIPO_TABLA", tipoMaestras);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);			

			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				maestras.setcodResultado((String) cstm.getObject("S_CODIGO_RESULTADO"));			
				
							
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					listmaestras listmaestras = new listmaestras();
					listmaestras.setMaestraid(rs.getString("MAESTRA_ID"));
					listmaestras.setDescripcion(rs.getString("DESCRIPCION_CORTA"));
					listmaestras.setDescripcionlarga(rs.getString("DESCRIPCION_LARGA"));

					
					maestras.getListmaestra().add(listmaestras);
				}
				response.setList(maestras);
				
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_MAESTRAS.SP_GET_LISTA_X_TIPO: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		
		return response;
	}

}
