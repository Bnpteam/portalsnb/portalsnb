package pe.gob.bnp.portalSNB.maestras.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.portalSNB.maestras.repository.maestrasRepository;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;

@Service
public class maestrasService {
	@Autowired
	maestrasRepository maestrasRepository;
	
	public ResponseTransactionSimple getMaestras(String tipoMaestras){
		ResponseTransactionSimple response = maestrasRepository.getMaestras(tipoMaestras);

		return response;
	} 
}
