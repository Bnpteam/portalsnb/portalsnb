package pe.gob.bnp.portalSNB.maestras.modal;

import java.util.ArrayList;
import java.util.List;

public class maestras {
	private String codResultado;
	private List<listmaestras> listmaestra;
	
	public maestras() {
		this.listmaestra = new ArrayList<>();
	}
	
	public String getcodResultado() {
		return codResultado;
	}
	public void setcodResultado(String codResultado) {
		this.codResultado = codResultado;
	}
	public List<listmaestras> getListmaestra() {
		return listmaestra;
	}
	public void setListmaestra(List<listmaestras> listmaestra) {
		this.listmaestra = listmaestra;
	}
	
}
