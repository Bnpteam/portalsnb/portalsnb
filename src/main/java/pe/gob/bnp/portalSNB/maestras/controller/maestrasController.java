package pe.gob.bnp.portalSNB.maestras.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.portalSNB.maestras.modal.listmaestras;
import pe.gob.bnp.portalSNB.maestras.service.maestrasService;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseHandler;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api")
@Api(value="/api/maestras")
@CrossOrigin(origins = "*")
public class maestrasController {
	@Autowired
	ResponseHandler responseHandler;
	@Autowired
	maestrasService maestrasService;
	
	@RequestMapping(method = RequestMethod.GET, path="/getMaestras",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Obtener maestras según tipo", response= listmaestras.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "tipoMaestro", defaultValue = "", required=false) String  tipoMaestro){
		ResponseTransactionSimple response = new ResponseTransactionSimple();
		try {
			response= maestrasService.getMaestras(tipoMaestro);
			if (response == null) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransactionSimple(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponseSimple(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponseSimple(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponseSimple(response,ex);
		}		
		
	}
}
