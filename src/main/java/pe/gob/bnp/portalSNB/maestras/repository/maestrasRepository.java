package pe.gob.bnp.portalSNB.maestras.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.portalSNB.maestras.modal.maestras;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.repository.BaseRepository;

@Repository
public interface maestrasRepository extends BaseRepository<maestras> {
	public ResponseTransactionSimple getMaestras(String tipoMaestras);

}
