package pe.gob.bnp.portalSNB.maestras.modal;

public class listmaestras {
	 private String maestraid;
	 private String descripcion;
	 private String descripcionlarga;
	public String getMaestraid() {
		return maestraid;
	}
	public void setMaestraid(String maestraid) {
		this.maestraid = maestraid;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcionlarga() {
		return descripcionlarga;
	}
	public void setDescripcionlarga(String descripcionlarga) {
		this.descripcionlarga = descripcionlarga;
	}
 
 
}
