package pe.gob.bnp.portalSNB.admin.repository;

import pe.gob.bnp.portalSNB.admin.dto.FiltroPublicacionesRequest;
import pe.gob.bnp.portalSNB.admin.dto.MensajeRequest;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.repository.BaseRepository;

public interface AdminRepository extends BaseRepository<Publicaciones>{	
	
	public ResponseTransaction updateObservar(Long id, MensajeRequest mensajeRequest);
	
	public ResponseTransaction updateAprobar(Long id,MensajeRequest mensajeRequest);
	
	public ResponseTransaction readObservaciones(Long id);
	
	public ResponseTransaction listPublicaciones(FiltroPublicacionesRequest publicacionesRequest);

}
