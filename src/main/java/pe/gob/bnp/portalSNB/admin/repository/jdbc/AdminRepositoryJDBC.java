package pe.gob.bnp.portalSNB.admin.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.portalSNB.admin.dto.EmailListResponse;
import pe.gob.bnp.portalSNB.admin.dto.FiltroPublicacionesRequest;
import pe.gob.bnp.portalSNB.admin.dto.MensajeRequest;
import pe.gob.bnp.portalSNB.admin.dto.ObservacionesListResponse;
import pe.gob.bnp.portalSNB.admin.dto.PublicacionesListResponse;
import pe.gob.bnp.portalSNB.admin.repository.AdminRepository;
import pe.gob.bnp.portalSNB.publicaciones.model.Publicaciones;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.repository.jdbc.BaseJDBCOperation;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;


@Repository
public class AdminRepositoryJDBC  extends BaseJDBCOperation implements AdminRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(AdminRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";


	@Override
	public ResponseTransaction persist(Publicaciones entity) {
		// TODO Auto-generated method stub
		return null;
	} 

	@Override
	public ResponseTransaction update(Publicaciones entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction updateObservar(Long id,MensajeRequest mensajeRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_ADMIN.SP_OBSERVAR(?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		 List<Object> resourcesResponse = new ArrayList<>();
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID",id);
			cstm.setString("P_MENSAJE", Utility.getString(mensajeRequest.getMensaje()));		
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(mensajeRequest.getUsuarioRegistroId()));				
			cstm.registerOutParameter("S_OBSERVA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long idPublicacion=  Utility.parseObjectToLong(cstm.getObject("S_OBSERVA_ID"));
				response.setId(String.valueOf(idPublicacion));
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
					if (rs.next()) {					
						EmailListResponse resourceResponse = new EmailListResponse();
						resourceResponse.setId(Utility.parseLongToString(rs.getLong("PUBLICACION_ID")));						
						resourceResponse.setTitulo(Utility.getString(rs.getString("PUBLICACION_TITULO")));					
						resourceResponse.setCorreo(Utility.getString(rs.getString("REGISTRADOR_CORREO")));	
						resourceResponse.setNombre(Utility.getString(rs.getString("REGISTRADOR_NOMBRE")));	
						resourceResponse.setFlagActivo(Utility.getString(rs.getString("ACTIVO_EMAIL")));	
										
						resourcesResponse.add(resourceResponse);
					}
					response.setList(resourcesResponse);
				
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_ADMIN.SP_OBSERVAR: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction updateAprobar(Long id,MensajeRequest mensajeRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_ADMIN.SP_APROBAR(?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID",id);
			cstm.setString("P_MENSAJE", Utility.getString(mensajeRequest.getMensaje()));		
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(mensajeRequest.getUsuarioRegistroId()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_OBSERVA_ID",OracleTypes.NUMBER);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long idPublicacion=  Utility.parseObjectToLong(cstm.getObject("S_OBSERVA_ID"));
				response.setId(String.valueOf(idPublicacion));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_ADMIN.SP_APROBAR: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction readObservaciones(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_ADMIN.SP_LIST_OBSERVACIONES(?,?,?)}";
             
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_PUBLICACION_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					ObservacionesListResponse resourceResponse = new ObservacionesListResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("OBSERVACION_ID")));
					resourceResponse.setIdPublicacion(Utility.parseLongToString(rs.getLong("PUBLICACION_ID")));
					resourceResponse.setMensaje(Utility.getString(rs.getString("OBSERVACION_MENSAJE")));					
					resourceResponse.setFechaRegistro(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_REGISTRO"))));
									
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_ADMIN.SP_LIST_OBSERVACIONES: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			System.out.println(e);
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction listPublicaciones(FiltroPublicacionesRequest publicacionesRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_ADMIN.SP_LIST_PUBLICACIONES(?,?,?,?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_TITULO", Utility.getString(publicacionesRequest.getTitulo()));			
			cstm.setString("P_CODDEPARTAMENTO",Utility.getString(publicacionesRequest.getCodeDepartamento()));		
			cstm.setLong("P_ESTADO_ID",Utility.parseStringToLong(publicacionesRequest.getEstadoId()));	
			cstm.setLong("P_TIPOPUBLICACION_ID",Utility.parseStringToLong(publicacionesRequest.getIdTipoPublicacion()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {	
					PublicacionesListResponse publicacionesResponse = new PublicacionesListResponse();
					publicacionesResponse.setId(Utility.parseLongToString(rs.getLong("PUBLICACION_ID")));
					publicacionesResponse.setTitulo(Utility.getString(rs.getString("PUBLICACION_TITULO")));
					publicacionesResponse.setDetalle(Utility.getString(rs.getString("PUBLICACION_DETALLE")));
					publicacionesResponse.setIdBiblioteca(Utility.parseLongToString(rs.getLong("ID_BIBLIOTECA")));
					publicacionesResponse.setNombreBiblioteca(Utility.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					publicacionesResponse.setHoraInicio(Utility.getString(rs.getString("HORA_INICIO")));
					publicacionesResponse.setHoraFin(Utility.getString(rs.getString("HORA_FIN")));
					publicacionesResponse.setFechaInicio(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_INICIO"))));
					publicacionesResponse.setFechaFin(Utility.parseDateToString(Utility.getDate(rs.getDate("FECHA_FIN"))));
					publicacionesResponse.setCodeDepartamento(Utility.getString(rs.getString("CODE_DEPARTAMENTO")));
					publicacionesResponse.setDepartamento(Utility.getString(rs.getString("DEPARTAMENTO")));
					publicacionesResponse.setFoto(Utility.getString(rs.getString("LINK_FOTO")));
					publicacionesResponse.setIdEstado(Utility.parseLongToString(rs.getLong("ESTADO_ID")));
					publicacionesResponse.setEstado(Utility.getString(rs.getString("ESTADO_DESCRIPCION")));
					publicacionesResponse.setIdTipoPublicacion(Utility.parseLongToString(rs.getLong("TIPOPUBLICACION_ID")));
					publicacionesResponse.setTipoPublicacion(Utility.getString(rs.getString("TIPOPUBLICACION_DESCRIPCION")));
					slidersResponse.add(publicacionesResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_ADMIN.SP_LIST_PUBLICACIONES: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {			
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

}
