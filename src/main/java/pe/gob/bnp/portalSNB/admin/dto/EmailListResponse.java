package pe.gob.bnp.portalSNB.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailListResponse {
	
	private String id;
	private String titulo;
	private String correo;
	private String nombre;
	private String flagActivo;

}
