package pe.gob.bnp.portalSNB.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FiltroPublicacionesRequest {

	private String idTipoPublicacion;

	private String codeDepartamento;

	private String titulo;

	private String estadoId;

}
