package pe.gob.bnp.portalSNB.admin.exception;

import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;

public class AdminException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";	
	public static final  String error0001 ="El email ingresado no existe.";
	public static final  String error0003Update ="El usuario esta inactivo.";
	
	public static ResponseTransaction setMessageResponseUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Update);
		}

		return response;
	} 
	
	public static ResponseTransaction setMessageAllList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		return response;
	}

}
