package pe.gob.bnp.portalSNB.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObservacionesListResponse {
	
	private String id;
	
	private String idPublicacion;
	
	private String mensaje;
	
	private String fechaRegistro;
	
	

}
