package pe.gob.bnp.portalSNB.admin.service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import pe.gob.bnp.portalSNB.utilitary.common.MailContentBuilder;


@Service
public class EmailService {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	private JavaMailSenderImpl emailSender;

	private MailContentBuilder mailContentBuilder;

	@Autowired
	public EmailService(JavaMailSenderImpl emailSender, MailContentBuilder mailContentBuilder) {
		this.emailSender = emailSender;
		this.mailContentBuilder = mailContentBuilder;
	}
	
	public void sendHTMLEmailObservaciones(String emailRegistrador,String userFullName, String titulo) {
		try {
			String subject="Observaciones de Publicaciones en el Sistema Nacional de Bibliotecas BNP";
			String fromEmail="notificaciones@bnp.gob.pe";
			String toEmail=emailRegistrador.trim().toString();
			
			int year = LocalDate.now().getYear();
			String anio=year+"";
			
			
			MimeMessagePreparator messagePreparator = mimeMessage -> {
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
				messageHelper.setFrom(fromEmail);
				messageHelper.setTo(toEmail);
				messageHelper.setSubject(subject);				
				String content = this.mailContentBuilder.buildTemplateObservaciones(userFullName,titulo,anio);
				messageHelper.setText(content, true);
			};
			this.emailSender.setDefaultEncoding(StandardCharsets.UTF_8.name());
			this.emailSender.send(messagePreparator);
			
		} catch (MailException e) {
			logger.error("sendHTMLEmailRegisterUser: "+e.getMessage());
		}
		
	}

}
