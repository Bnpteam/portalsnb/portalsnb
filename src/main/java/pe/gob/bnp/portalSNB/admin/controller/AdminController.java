package pe.gob.bnp.portalSNB.admin.controller;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.portalSNB.admin.dto.EmailListResponse;
import pe.gob.bnp.portalSNB.admin.dto.MensajeRequest;
import pe.gob.bnp.portalSNB.admin.dto.ObservacionesListResponse;
import pe.gob.bnp.portalSNB.admin.dto.PublicacionesListResponse;
import pe.gob.bnp.portalSNB.admin.service.AdminService;
import pe.gob.bnp.portalSNB.admin.service.EmailService;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseHandler;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api/admin")
@Api(value = "/api/admin")
@CrossOrigin(origins = "*")
public class AdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);		

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	AdminService adminService;
	
	@Autowired
	EmailService  emailService;
	
	
	/*
	 * OBSERVAR O APROBAR
	 */
	

	@RequestMapping(method = RequestMethod.PUT,path="/observar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "observar noticia o evento", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> updateObservar(
			@ApiParam(value = "id Publicacion", required = true)
			@PathVariable Long id,@RequestBody MensajeRequest mensajeRequest) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = adminService.updateObservar(id,mensajeRequest);
			
			if(response!=null && response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				if(response.getList()!=null && response.getList().size()>0) {
					EmailListResponse emailListResponse=(EmailListResponse)response.getList().get(0);
					
					if(emailListResponse.getFlagActivo().equalsIgnoreCase("1"))
						emailService.sendHTMLEmailObservaciones(emailListResponse.getCorreo(),emailListResponse.getNombre(),emailListResponse.getTitulo());
					 List<Object> resourcesResponse = new ArrayList<>();
					response.setList(resourcesResponse);
				}			
			}
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/observar/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/observar/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.PUT,path="/aprobar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "aprobar noticia o evento", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> updateAprobar(
			@ApiParam(value = "id publicacion", required = true)
			@PathVariable Long id,@RequestBody MensajeRequest mensajeRequest) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = adminService.updateAprobar(id,mensajeRequest);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/aprobar/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/aprobar/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.GET, path="/observaciones/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "obtener observaciones x Id", response= ObservacionesListResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id publicacion", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = adminService.getObservaciones(id);
			if (response == null || response.getList().size()==0) {
				logger.info("/observaciones/"+id+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/observaciones/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/observaciones/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/observaciones/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, path="/publicaciones",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar publicaciones BNP", response= PublicacionesListResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "titulo", defaultValue = "", required=false) String  titulo,
			@RequestParam(value = "codedepartamento", defaultValue = "", required=false) String  codedepartamento,
			@RequestParam(value = "idEstado", defaultValue = "", required=false) String  idEstado,
			@RequestParam(value = "idTipoPublicacion", defaultValue = "", required=false) String  idTipoPublicacion){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = adminService.searchPublicaciones(titulo,codedepartamento,idEstado,idTipoPublicacion);
			if (response == null || response.getList().size()==0 ) {
				logger.info("/publicaciones titulo:"+titulo+", codedepartamento: "+codedepartamento+"No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/publicaciones titulo:"+titulo+", codedepartamento: "+codedepartamento+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/publicaciones titulo:"+titulo+", codedepartamento: "+codedepartamento+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/publicaciones titulo:"+titulo+", codedepartamento: "+codedepartamento+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	

}
