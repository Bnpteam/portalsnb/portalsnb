package pe.gob.bnp.portalSNB.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.portalSNB.admin.dto.FiltroPublicacionesRequest;
import pe.gob.bnp.portalSNB.admin.dto.MensajeRequest;
import pe.gob.bnp.portalSNB.admin.exception.AdminException;
import pe.gob.bnp.portalSNB.admin.repository.AdminRepository;
import pe.gob.bnp.portalSNB.admin.validation.AdminValidation;
import pe.gob.bnp.portalSNB.utilitary.common.Notification;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;


@Service
public class AdminService {
	
	@Autowired
	AdminRepository adminRepository;
	
	public ResponseTransaction updateObservar(Long id, MensajeRequest mensajeRequest) {

		Notification notification = AdminValidation.validationUpdate(mensajeRequest);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		
		
		response = adminRepository.updateObservar(id,mensajeRequest);	
		
		return AdminException.setMessageResponseUpdate(response);

	}
	
	
	public ResponseTransaction updateAprobar(Long id, MensajeRequest mensajeRequest) {

		Notification notification = AdminValidation.validationUpdate(mensajeRequest);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		
		
		response = adminRepository.updateAprobar(id,mensajeRequest);	
		
		return AdminException.setMessageResponseUpdate(response);

	}
	
	public ResponseTransaction getObservaciones(Long id)  {
		
		ResponseTransaction response = adminRepository.readObservaciones(id);

		return response;
	}
	
	public ResponseTransaction searchPublicaciones(String titulo, String codedepartamento,String idEstado,String idTipoPublicacion){	

		ResponseTransaction response = new ResponseTransaction();	
		
		FiltroPublicacionesRequest filtroAllRequest = new FiltroPublicacionesRequest ();		
		filtroAllRequest.setTitulo(titulo);
		filtroAllRequest.setCodeDepartamento(codedepartamento);
		filtroAllRequest.setEstadoId(idEstado);
		filtroAllRequest.setIdTipoPublicacion(idTipoPublicacion);
		
		response=adminRepository.listPublicaciones(filtroAllRequest);
		
		return AdminException.setMessageAllList(response);	
	}

}
