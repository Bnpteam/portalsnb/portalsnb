package pe.gob.bnp.portalSNB.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MensajeRequest {

	private String mensaje;

	private String usuarioRegistroId;

}
