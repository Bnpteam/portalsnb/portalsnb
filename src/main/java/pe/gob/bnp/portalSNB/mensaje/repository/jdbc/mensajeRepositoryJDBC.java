package pe.gob.bnp.portalSNB.mensaje.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.portalSNB.mensaje.dto.MensajeResponse;
import pe.gob.bnp.portalSNB.mensaje.dto.mensajeDTO;
import pe.gob.bnp.portalSNB.mensaje.repository.mensajeRepository;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.common.Utility;
import pe.gob.bnp.portalSNB.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class mensajeRepositoryJDBC extends BaseJDBCOperation implements mensajeRepository  {
	
	private static final Logger logger = LoggerFactory.getLogger(mensajeRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(mensajeDTO entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction update(mensajeDTO entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransactionSimple registrarMensaje(mensajeDTO mensajeDTO) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MENSAJES.SP_INSERTAR_MENSAJE(?,?,?,?,?,?,?)}";
        ResponseTransactionSimple response = new ResponseTransactionSimple();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("CORREO_REMITENTE", mensajeDTO.getCORREO_REMITENTE());
			cstm.setString("ASUNTO", mensajeDTO.getASUNTO());
			cstm.setString("TELEFONO", mensajeDTO.getTELEFONO());
			cstm.setString("DETALLE", mensajeDTO.getDETALLE());
			cstm.setInt("ID_TIPO_MENSAJE", mensajeDTO.getID_TIPO_MENSAJE());
			cstm.setString("ID_TIPO_MENSAJE_DETALLE", mensajeDTO.getID_TIPO_MENSAJE_DETALLE());
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);

			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {								
				response.setResponse("Registro Exitoso");			
				
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_MENSAJES.SP_INSERTAR_MENSAJE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		
		return response;
		
		
	}

	@Override
	public ResponseTransaction listAll(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MENSAJES.SP_LIST_MENSAJES(?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_USUARIO_ID_REGISTRO",id);		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {	
					MensajeResponse eventosResponse = new MensajeResponse();
					eventosResponse.setMensajeId(Utility.parseLongToString(rs.getLong("MENSAJE_ID")));
					eventosResponse.setMensajeDetalle(Utility.getString(rs.getString("MENSAJE_DETALLE")));
					eventosResponse.setMensajeAsunto(Utility.getString(rs.getString("MENSAJE_ASUNTO")));
					eventosResponse.setMensajeCorreo(Utility.getString(rs.getString("MENSAJE_CORREO")));
					eventosResponse.setMensajeTelefono(Utility.getString(rs.getString("MENSAJE_TELEFONO")));
					eventosResponse.setMensajeTipoId(Utility.getString(rs.getString("MENSAJE_ID_TIPO")));
					eventosResponse.setMensajeTipoNombre(Utility.getString(rs.getString("MENSAJE_NOMBRE_TIPO")));
					eventosResponse.setMensajeRespuesta(Utility.getString(rs.getString("MENSAJE_RESPUESTA")));
					
					slidersResponse.add(eventosResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_MENSAJES.SP_LIST_MENSAJES: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
			
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	
}
