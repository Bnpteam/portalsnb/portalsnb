package pe.gob.bnp.portalSNB.mensaje.dto;

public class mensajeDTO {
	private Integer ID_MENSAJE;
	private String DETALLE;
	private String ASUNTO;
	private String FECHA_ENVIO;
	private String FECHA_RESPUESTA;
	private String CORREO_REMITENTE;
	private String TELEFONO;
	private String FECHA_REGISTRO;
	private Integer USUARIO_ID_REGISTRO;
	private String FECHA_MODIFICACION;
	private String USUARIO_ID_MODIFICACION;
	private Integer ID_TIPO_MENSAJE;
	private String ID_TIPO_MENSAJE_DETALLE;
	private Integer ESTADO;
	private String RESPUESTA;
	private String REMITENTE;
	
	public Integer getID_MENSAJE() {
		return ID_MENSAJE;
	}
	public void setID_MENSAJE(Integer iD_MENSAJE) {
		ID_MENSAJE = iD_MENSAJE;
	}
	public String getDETALLE() {
		return DETALLE;
	}
	public void setDETALLE(String dETALLE) {
		DETALLE = dETALLE;
	}
	public String getASUNTO() {
		return ASUNTO;
	}
	public void setASUNTO(String aSUNTO) {
		ASUNTO = aSUNTO;
	}
	public String getFECHA_ENVIO() {
		return FECHA_ENVIO;
	}
	public void setFECHA_ENVIO(String fECHA_ENVIO) {
		FECHA_ENVIO = fECHA_ENVIO;
	}
	public String getFECHA_RESPUESTA() {
		return FECHA_RESPUESTA;
	}
	public void setFECHA_RESPUESTA(String fECHA_RESPUESTA) {
		FECHA_RESPUESTA = fECHA_RESPUESTA;
	}
	public String getCORREO_REMITENTE() {
		return CORREO_REMITENTE;
	}
	public void setCORREO_REMITENTE(String cORREO_REMITENTE) {
		CORREO_REMITENTE = cORREO_REMITENTE;
	}
	public String getTELEFONO() {
		return TELEFONO;
	}
	public void setTELEFONO(String tELEFONO) {
		TELEFONO = tELEFONO;
	}
	public String getFECHA_REGISTRO() {
		return FECHA_REGISTRO;
	}
	public void setFECHA_REGISTRO(String fECHA_REGISTRO) {
		FECHA_REGISTRO = fECHA_REGISTRO;
	}
	public Integer getUSUARIO_ID_REGISTRO() {
		return USUARIO_ID_REGISTRO;
	}
	public void setUSUARIO_ID_REGISTRO(Integer uSUARIO_ID_REGISTRO) {
		USUARIO_ID_REGISTRO = uSUARIO_ID_REGISTRO;
	}
	public String getFECHA_MODIFICACION() {
		return FECHA_MODIFICACION;
	}
	public void setFECHA_MODIFICACION(String fECHA_MODIFICACION) {
		FECHA_MODIFICACION = fECHA_MODIFICACION;
	}
	public String getUSUARIO_ID_MODIFICACION() {
		return USUARIO_ID_MODIFICACION;
	}
	public void setUSUARIO_ID_MODIFICACION(String uSUARIO_ID_MODIFICACION) {
		USUARIO_ID_MODIFICACION = uSUARIO_ID_MODIFICACION;
	}
	public Integer getID_TIPO_MENSAJE() {
		return ID_TIPO_MENSAJE;
	}
	public void setID_TIPO_MENSAJE(Integer iD_TIPO_MENSAJE) {
		ID_TIPO_MENSAJE = iD_TIPO_MENSAJE;
	}
	public String getID_TIPO_MENSAJE_DETALLE() {
		return ID_TIPO_MENSAJE_DETALLE;
	}
	public void setID_TIPO_MENSAJE_DETALLE(String iD_TIPO_MENSAJE_DETALLE) {
		ID_TIPO_MENSAJE_DETALLE = iD_TIPO_MENSAJE_DETALLE;
	}
	public Integer getESTADO() {
		return ESTADO;
	}
	public void setESTADO(Integer eSTADO) {
		ESTADO = eSTADO;
	}
	public String getRESPUESTA() {
		return RESPUESTA;
	}
	public void setRESPUESTA(String rESPUESTA) {
		RESPUESTA = rESPUESTA;
	}
	public String getREMITENTE() {
		return REMITENTE;
	}
	public void setREMITENTE(String rEMITENTE) {
		REMITENTE = rEMITENTE;
	}
	
	

}
