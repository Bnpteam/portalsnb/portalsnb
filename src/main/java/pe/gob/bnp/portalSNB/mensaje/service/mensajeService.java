package pe.gob.bnp.portalSNB.mensaje.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.portalSNB.mensaje.dto.mensajeDTO;
import pe.gob.bnp.portalSNB.mensaje.repository.mensajeRepository;
import pe.gob.bnp.portalSNB.publicaciones.dto.FiltroAllRequest;
import pe.gob.bnp.portalSNB.publicaciones.exception.EventosException;
import pe.gob.bnp.portalSNB.usuario.exception.usuarioException;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;

@Service
public class mensajeService {
	@Autowired
	mensajeRepository mensajeRepository;
	
	public ResponseTransactionSimple enviarMensaje(mensajeDTO mensajeDTO){
		ResponseTransactionSimple response = mensajeRepository.registrarMensaje(mensajeDTO);		
		
		return usuarioException.setMessageResponseLogin(response);
	}
	
	public ResponseTransaction searchAll(Long idRegistrador){	

		ResponseTransaction response = new ResponseTransaction();		
		
		response=mensajeRepository.listAll(idRegistrador);
		
		return EventosException.setMessageAllList(response);	
	}

}
