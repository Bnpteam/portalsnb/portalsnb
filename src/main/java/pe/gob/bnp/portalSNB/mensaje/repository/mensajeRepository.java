package pe.gob.bnp.portalSNB.mensaje.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.portalSNB.mensaje.dto.mensajeDTO;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.repository.BaseRepository;

@Repository
public interface mensajeRepository extends BaseRepository<mensajeDTO> {
	public ResponseTransactionSimple registrarMensaje(mensajeDTO mensajeDTO);
	
	public ResponseTransaction listAll(Long id);

	
}
