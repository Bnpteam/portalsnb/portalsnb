package pe.gob.bnp.portalSNB.mensaje.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.portalSNB.mensaje.dto.mensajeDTO;
import pe.gob.bnp.portalSNB.mensaje.service.mensajeService;
import pe.gob.bnp.portalSNB.publicaciones.dto.EventosListResponse;
import pe.gob.bnp.portalSNB.usuario.controller.usuarioController;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseHandler;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;
import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;
import pe.gob.bnp.portalSNB.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api")
@Api(value="/api/mensaje")
@CrossOrigin(origins = "*")
public class mensajeController {
private static final Logger logger = LoggerFactory.getLogger(usuarioController.class);
	
	@Autowired
	ResponseHandler responseHandler;
	@Autowired
	mensajeService mensajeService;
	
	@RequestMapping(method = RequestMethod.POST, path = "/registrarMensaje", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Login de usuario, debe estar habilitado domain", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 400, message = "Solicitud contiene errores"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})
	public ResponseEntity<Object> registrarMensaje(
			@ApiParam(value = "Servicio para enviar mensaje", required = true)
			@RequestBody  mensajeDTO mensajeDTO){ 
		ResponseTransactionSimple response=new ResponseTransactionSimple();
		try {
			
			response = mensajeService.enviarMensaje(mensajeDTO);
			
			
			return this.responseHandler.getOkResponseTransactionSimple(response);
		} catch (IllegalArgumentException ex) {
			logger.error("login IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponseSimple(response,"Error Autenticacion "+ex.getMessage());
		} catch (Throwable ex) {
			logger.error("login Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponseSimple(response,ex);
		}
	
	}
	

	@RequestMapping(method = RequestMethod.GET, path="/mensajes/{idRegistrador}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar mensajes BNP", response= EventosListResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@ApiParam(value = "idRegistrador", required = true)
			@PathVariable Long idRegistrador){
		ResponseTransaction response=new ResponseTransaction();
		try { 
			response = mensajeService.searchAll(idRegistrador);
			if (response == null || response.getList().size()==0 ) {
				logger.info("/mensajes/idRegistrador/ "+idRegistrador+" No se encontraron registros.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/mensajes/idRegistrador/"+idRegistrador+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/mensajes/idRegistrador/"+idRegistrador+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/mensajes/idRegistrador/"+idRegistrador+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
}
