package pe.gob.bnp.portalSNB.mensaje.dto;

public class MensajeResponse {
	
	private String mensajeId;
	private String mensajeDetalle;
	private String mensajeAsunto;
	private String mensajeCorreo;
	private String mensajeTelefono;
	private String mensajeTipoId;
	private String mensajeTipoNombre;
	private String mensajeRespuesta;

	
	public String getMensajeId() {
		return mensajeId;
	}
	public void setMensajeId(String mensajeId) {
		this.mensajeId = mensajeId;
	}
	public String getMensajeDetalle() {
		return mensajeDetalle;
	}
	public void setMensajeDetalle(String mensajeDetalle) {
		this.mensajeDetalle = mensajeDetalle;
	}
	public String getMensajeAsunto() {
		return mensajeAsunto;
	}
	public void setMensajeAsunto(String mensajeAsunto) {
		this.mensajeAsunto = mensajeAsunto;
	}
	public String getMensajeCorreo() {
		return mensajeCorreo;
	}
	public void setMensajeCorreo(String mensajeCorreo) {
		this.mensajeCorreo = mensajeCorreo;
	}
	public String getMensajeTelefono() {
		return mensajeTelefono;
	}
	public void setMensajeTelefono(String mensajeTelefono) {
		this.mensajeTelefono = mensajeTelefono;
	}
	public String getMensajeTipoId() {
		return mensajeTipoId;
	}
	public void setMensajeTipoId(String mensajeTipoId) {
		this.mensajeTipoId = mensajeTipoId;
	}
	public String getMensajeTipoNombre() {
		return mensajeTipoNombre;
	}
	public void setMensajeTipoNombre(String mensajeTipoNombre) {
		this.mensajeTipoNombre = mensajeTipoNombre;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
}
