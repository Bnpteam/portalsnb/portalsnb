package pe.gob.bnp.portalSNB.utilitary.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import pe.gob.bnp.portalSNB.utilitary.dto.ResponseDto;
import pe.gob.bnp.portalSNB.utilitary.dto.ResponseOkCommandDto;
import pe.gob.bnp.portalSNB.utilitary.dto.ResponseTransactionDto;
import pe.gob.bnp.portalSNB.utilitary.dto.ResponseTransactionDtoSimple;

@Component
public class ResponseHandler {
	private  Logger logger = LoggerFactory.getLogger(ResponseHandler.class);
	private String codeNotFoundResponse="00404";
	private String codeAppCustomErrorResponse="00400";
	private String codeAppExceptionResponse="00500";
	
	public ResponseEntity<Object> getOkCommandResponse(String message) {
		ResponseDto responseDto = new ResponseDto();
		ResponseOkCommandDto responseOkCommandDto = new ResponseOkCommandDto();
		responseOkCommandDto.setHttpStatus(HttpStatus.OK.value());
		responseOkCommandDto.setMessage(message);
		responseDto.setResponse(responseOkCommandDto);
		return new ResponseEntity<Object>(responseDto.getResponse(), HttpStatus.OK);
	}	
	
	public ResponseEntity<Object> getOkObjectResponse(Object message) {
		return new ResponseEntity<Object>(message, HttpStatus.OK);
	}
	
	public ResponseEntity<Object> getOkResponseTransaction(ResponseTransaction responseTx) {
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.OK.value());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.OK);
	}	
	
	public ResponseEntity<Object> getOkResponseTransactionSimple(ResponseTransactionSimple responseTx) {
		ResponseTransactionDtoSimple responseTxDto = new ResponseTransactionDtoSimple();
		responseTxDto.setHttpStatus(HttpStatus.OK.value());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.OK);
	}
	
	
	public ResponseEntity<Object> getAppCustomErrorResponse(ResponseTransaction responseTx,String errorMessages) {
		
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.BAD_REQUEST.value());
		responseTx.setCodeResponse(codeAppCustomErrorResponse);
		responseTx.setResponse(errorMessages);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.BAD_REQUEST);
	}
	
public ResponseEntity<Object> getAppCustomErrorResponseSimple(ResponseTransactionSimple responseTx,String errorMessages) {
		
		ResponseTransactionDtoSimple responseTxDto = new ResponseTransactionDtoSimple();
		responseTxDto.setHttpStatus(HttpStatus.BAD_REQUEST.value());
		responseTx.setCodeResponse(codeAppCustomErrorResponse);
		responseTx.setResponse(errorMessages);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.BAD_REQUEST);
	}
	
	
	public ResponseEntity<Object> getAppExceptionResponse(ResponseTransaction responseTx,Throwable e) {
		logger.error(e.getMessage(), e);

		
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		responseTx.setCodeResponse(codeAppExceptionResponse);
		responseTx.setResponse("Error Server:"+e.getMessage());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}		
	
	public ResponseEntity<Object> getAppExceptionResponseSimple(ResponseTransactionSimple responseTx,Throwable e) {
		logger.error(e.getMessage(), e);

		
		ResponseTransactionDtoSimple responseTxDto = new ResponseTransactionDtoSimple();
		responseTxDto.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		responseTx.setCodeResponse(codeAppExceptionResponse);
		responseTx.setResponse("Error Server:"+e.getMessage());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}		
	
	public ResponseEntity<Object> getNotFoundObjectResponse(ResponseTransaction responseTx,String message) {
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		//responseTxDto.setHttpStatus(HttpStatus.NOT_FOUND.value());
		responseTxDto.setHttpStatus(HttpStatus.OK.value());
		responseTx.setCodeResponse(codeNotFoundResponse);
		//responseTx.setResponse("Not Found:"+message);
		responseTx.setResponse(message);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.OK);
	}	
	public ResponseEntity<Object> getNotFoundObjectResponse(ResponseTransactionSimple responseTx,String message) {
		ResponseTransactionDtoSimple responseTxDto = new ResponseTransactionDtoSimple();
		//responseTxDto.setHttpStatus(HttpStatus.NOT_FOUND.value());
		responseTxDto.setHttpStatus(HttpStatus.OK.value());
		responseTx.setCodeResponse(codeNotFoundResponse);
		//responseTx.setResponse("Not Found:"+message);
		responseTx.setResponse(message);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.OK);
	}	
	public ResponseEntity<Object> getNotFoundObjectResponseSimple(ResponseTransactionSimple responseTx,String message) {
		ResponseTransactionDtoSimple responseTxDto = new ResponseTransactionDtoSimple();
		//responseTxDto.setHttpStatus(HttpStatus.NOT_FOUND.value());
		responseTxDto.setHttpStatus(HttpStatus.OK.value());
		responseTx.setCodeResponse(codeNotFoundResponse);
		//responseTx.setResponse("Not Found:"+message);
		responseTx.setResponse(message);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto,  HttpStatus.OK);
	}
	
}
