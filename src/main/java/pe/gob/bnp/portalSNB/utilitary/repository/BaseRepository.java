package pe.gob.bnp.portalSNB.utilitary.repository;


import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransaction;

public interface BaseRepository<T> {
	
	public ResponseTransaction persist(T entity);
	public ResponseTransaction update(T entity);

}
