package pe.gob.bnp.portalSNB.utilitary.repository.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OracleConnection {
	
	private static final Logger logger = LoggerFactory.getLogger(OracleConnection.class);
		
	private static String ipDB;		

	private static String userDB;

	private static String passwordDB;

	private static String schemaDB;
	
	private static String catchConecctionJDBC="getConnectionJDBC ";	
	
	public static String getIpDB() {
		return ipDB;
	}

	@Value("${portalsnb.db.ip}")
	public void setIpDB(String ipDB) {
		OracleConnection.ipDB = ipDB;
	}	
	
	public static String getUserDB() {
		return userDB;
	}

	@Value("${portalsnb.db.usuario}")
	public void setUserDB(String userDB) {
		OracleConnection.userDB = userDB;
	}

	public static String getPasswordDB() {
		return passwordDB;
	}

	@Value("${portalsnb.db.password}")
	public void setPasswordDB(String passwordDB) {
		OracleConnection.passwordDB = passwordDB;
	}

	public static String getSchemaDB() {
		return schemaDB;
	}

	@Value("${portalsnb.db.schema}")
	public void setSchemaDB(String schemaDB) {
		OracleConnection.schemaDB = schemaDB;
	}

	public static Connection getConnectionJDBC() throws Exception {
		String connectionURL = "jdbc:oracle:thin:@" + ipDB + ":1521:" + schemaDB;	
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(connectionURL, userDB,passwordDB);
			connection.setAutoCommit(false);
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error(catchConecctionJDBC + exception.getMessage(), exception);
		}
		return connection;
	}

}
