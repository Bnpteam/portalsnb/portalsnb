package pe.gob.bnp.portalSNB.utilitary.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {

	private TemplateEngine templateEngine;

	@Autowired
	public MailContentBuilder(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}
	
	public String buildTemplateObservaciones(String userFullName,String titulo, String anio) {
		Context context = new Context();
		context.setVariable("NAME", userFullName);
		context.setVariable("TITULO", titulo);
		context.setVariable("YEAR", anio);
		return templateEngine.process("observaciones", context);
	}
	

}
