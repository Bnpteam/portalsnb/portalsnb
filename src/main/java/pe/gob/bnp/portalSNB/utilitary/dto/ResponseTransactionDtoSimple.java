package pe.gob.bnp.portalSNB.utilitary.dto;

import pe.gob.bnp.portalSNB.utilitary.common.ResponseTransactionSimple;

public class ResponseTransactionDtoSimple {
	
	private int httpStatus;
	private ResponseTransactionSimple response;
	
	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public ResponseTransactionSimple getResponse() {
		return response;
	}

	public void setResponse(ResponseTransactionSimple response) {
		this.response = response;
	}
}
