package pe.gob.bnp.portalSNB.utilitary.common;

import java.util.ArrayList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="ResponseTransactionSimple", description="transaction response simple")

public class ResponseTransactionSimple {

	@ApiModelProperty(notes = "id",required=false,value="100")
	private String id;
	
	@ApiModelProperty(notes = "code response",required=true,value="0000")
	private String codeResponse;
	
	@ApiModelProperty(notes = "description response",required=true,value="Tansacción finalizó con éxito.")
	private String response;
	
	@ApiModelProperty(notes = "object",required=true,value="No hay registros.")
	private Object list;	
	

	public ResponseTransactionSimple() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.codeResponse = Constants.EMPTY_STRING;
		this.response = Constants.EMPTY_STRING;
		this.list=new ArrayList<Object>();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCodeResponse() {
		return codeResponse;
	}

	public void setCodeResponse(String codeResponse) {
		this.codeResponse = codeResponse;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	public Object getList() {
		return list;
	}

	public void setList(Object list) {
		this.list = list;
	}
}
