## SERVICIOS NOTICIAS Y EVENTOS PORTAL SNB
### Descripción
- Backend de servicios crud para noticias y eventos del Portal SNB.
- Desde la página principal del SNB https://snb.gob.pe/#/ se puede visualizar los eventos y noticias principales aprobadas por el usuario registrador asociado
  al sistema RNB.( se puede listar noticias o eventos aprobados, noticas o eventos detallados finalmente crear o actualizar noticias,eventos).

### Configuración

- Para configurar en el ambientes de TEST:

 * Dentro del Proyecto portalSNB cambiar parametro spring.profiles.active a "development" en el archivo application.properties.
 * Dentro del Proyecto portalSNB cambiar archivo DataSource parametros del pool de conexion BD de Test.
 * Desactivar ssl archivo principal PortalSNBApplication y parametros server.ssl.
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA: http://rnb.bnp.gob.pe:8090/portalsnb/swagger-ui.html#/
 
- Para configurar en el ambiente de PROD:

 * Dentro del Proyecto portalSNB cambiar parametro spring.profiles.active a "production" en el archivo application.properties.
 * Dentro del Proyecto portalSNB cambiar archivo DataSource parametros del pool de conexion BD de Prod.
 * Activar ssl archivo principal PortalSNBApplication y parametros server.ssl.
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA: https://apiportalsnb.bnp.gob.pe:9883/portalsnb/swagger-ui.html#/

 ### Despliegue Microservicio

- Utilizar el archivo portalsnb-be.service como base para desplegar el microservicio
- Pasos de despliegue:
    1. Generar jar del aplicativo portalsnb (mave clean build install).
    2. Copiar el jar dentro del servidor test o prod que desee en la ruta indicada por el archivo portalsnb-be.service. (dar permisos escritura y lectura al jar)
    3. Generar en caso no exista un servicio en linux con el nombre del archivo portalsnb-be.service.
        * Para crear servicio en linux, acceder a la ruta /etc/systemd validar los servicios existentes y copiar con el comando "cp" servicio existente actualizar       las rutas para que apunten al jar creado. 
    4. Una vez creado o actualizado el archivo .service reiniciarlo de preferencia utilizar los comando:
        systemctl status portalsnb-be.service (ver el estado actual)
        systemctl stop   portalsnb-be.service (detener servicio)
        systemctl start  portalsnb-be.service (inciar servicio)
    
    5. Finalmente validar los servicios desde la ruta generado por el swagger.

 
#### version: v1.0.2